OnePlus Dolby
==============

OnePlus Dolby Atmos that supports Dolby Digital Plus codecs. [AC3 | EAC3 | EAC3-JOC]

Getting Started

---------------
Clone the repository :-

```bash
git clone https://gitlab.com/someone5678/vendor_dolby.git vendor/dolby
```

Patches required on the device tree side (Example) :-

[sdm660-common: Adapted to OP Dolby configuration [SQUASH]](https://gitlab.com/someone5678/vendor_dolby/-/commit/72dd1e0d5fdc1d7d3f2ee4a368e080a60b80b2d3)

To build, add the dolby effects in your device's audio effects config then inherit the dolby config by adding this in your device's makefile :-

```bash
$(call inherit-product-if-exists, vendor/dolby/dolby.mk)
```
