#!/bin/bash

if [ -z $VENDOR_DOLBY_SETUP_DONE ]; then

APKEDITOR=$(pwd)/prebuilts/tools-parasite/common/bin/APKEditor

cd vendor/dolby

if [ -d "custom/DaxUICustom" ]; then
    cd custom/DaxUICustom
    $APKEDITOR b -i DaxUI -f -o DaxUICustom.apk

    cp -rf DaxUI/AndroidManifest.xml AndroidManifest.xml.bak
    sed -i '/android.intent.category.LAUNCHER/d' DaxUI/AndroidManifest.xml
    $APKEDITOR b -i DaxUI -f -o DaxUICustom-NoLauncher.apk
    rm DaxUI/AndroidManifest.xml
    mv AndroidManifest.xml.bak DaxUI/AndroidManifest.xml
    cd ../../
fi

cd ../../

export VENDOR_DOLBY_SETUP_DONE=true
fi
