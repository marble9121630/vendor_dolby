.class public final Landroid/support/v17/leanback/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final lb_page_indicator_arrow_gap:I = 0x7f06012b # @dimen/lb_page_indicator_arrow_gap '32.0dp'

.field public static final lb_page_indicator_arrow_radius:I = 0x7f06012c # @dimen/lb_page_indicator_arrow_radius '18.0dp'

.field public static final lb_page_indicator_arrow_shadow_offset:I = 0x7f06012d # @dimen/lb_page_indicator_arrow_shadow_offset '1.0dp'

.field public static final lb_page_indicator_arrow_shadow_radius:I = 0x7f06012e # @dimen/lb_page_indicator_arrow_shadow_radius '2.0dp'

.field public static final lb_page_indicator_dot_gap:I = 0x7f06012f # @dimen/lb_page_indicator_dot_gap '16.0dp'

.field public static final lb_page_indicator_dot_radius:I = 0x7f060130 # @dimen/lb_page_indicator_dot_radius '5.0dp'

.field public static final lb_playback_transport_hero_thumbs_height:I = 0x7f060155 # @dimen/lb_playback_transport_hero_thumbs_height '192.0dp'

.field public static final lb_playback_transport_hero_thumbs_width:I = 0x7f060156 # @dimen/lb_playback_transport_hero_thumbs_width '192.0dp'

.field public static final lb_playback_transport_progressbar_active_bar_height:I = 0x7f060159 # @dimen/lb_playback_transport_progressbar_active_bar_height '6.0dp'

.field public static final lb_playback_transport_progressbar_active_radius:I = 0x7f06015a # @dimen/lb_playback_transport_progressbar_active_radius '6.0dp'

.field public static final lb_playback_transport_progressbar_bar_height:I = 0x7f06015b # @dimen/lb_playback_transport_progressbar_bar_height '4.0dp'

.field public static final lb_playback_transport_thumbs_height:I = 0x7f06015e # @dimen/lb_playback_transport_thumbs_height '154.0dp'

.field public static final lb_playback_transport_thumbs_margin:I = 0x7f06015f # @dimen/lb_playback_transport_thumbs_margin '4.0dp'

.field public static final lb_playback_transport_thumbs_width:I = 0x7f060160 # @dimen/lb_playback_transport_thumbs_width '154.0dp'

.field public static final lb_search_bar_height:I = 0x7f060165 # @dimen/lb_search_bar_height '60.0dp'

.field public static final lb_search_orb_focused_z:I = 0x7f060178 # @dimen/lb_search_orb_focused_z '8.0dp'

.field public static final lb_search_orb_unfocused_z:I = 0x7f06017e # @dimen/lb_search_orb_unfocused_z '2.0dp'

.field public static final picker_item_height:I = 0x7f0601c2 # @dimen/picker_item_height '32.0dp'
