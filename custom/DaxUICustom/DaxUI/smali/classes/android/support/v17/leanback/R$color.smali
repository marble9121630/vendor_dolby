.class public final Landroid/support/v17/leanback/R$color;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final lb_default_search_color:I = 0x7f050081 # @color/lb_default_search_color '#ff86c739'

.field public static final lb_page_indicator_arrow_background:I = 0x7f05008f # @color/lb_page_indicator_arrow_background '#ffeeeeee'

.field public static final lb_page_indicator_arrow_shadow:I = 0x7f050090 # @color/lb_page_indicator_arrow_shadow '#4c000000'

.field public static final lb_page_indicator_dot:I = 0x7f050091 # @color/lb_page_indicator_dot '#ff014269'

.field public static final lb_playback_media_row_highlight_color:I = 0x7f050097 # @color/lb_playback_media_row_highlight_color '#1affffff'

.field public static final lb_search_bar_hint:I = 0x7f05009c # @color/lb_search_bar_hint '#ff888888'

.field public static final lb_search_bar_hint_speech_mode:I = 0x7f05009d # @color/lb_search_bar_hint_speech_mode '#66222222'

.field public static final lb_search_bar_text:I = 0x7f05009e # @color/lb_search_bar_text '#80eeeeee'

.field public static final lb_search_bar_text_speech_mode:I = 0x7f05009f # @color/lb_search_bar_text_speech_mode '#ff444444'

.field public static final lb_speech_orb_not_recording:I = 0x7f0500a1 # @color/lb_speech_orb_not_recording '#ffcccccc'

.field public static final lb_speech_orb_not_recording_icon:I = 0x7f0500a2 # @color/lb_speech_orb_not_recording_icon '#ff555555'

.field public static final lb_speech_orb_not_recording_pulsed:I = 0x7f0500a3 # @color/lb_speech_orb_not_recording_pulsed '#ffeeeeee'

.field public static final lb_speech_orb_recording:I = 0x7f0500a4 # @color/lb_speech_orb_recording '#ffff4343'
