.class public final Landroid/support/v17/leanback/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final lb_picker:I = 0x7f0c0056 # @layout/lb_picker 'res/layout/lb_picker.xml'

.field public static final lb_picker_column:I = 0x7f0c0057 # @layout/lb_picker_column 'res/layout/lb_picker_column.xml'

.field public static final lb_picker_item:I = 0x7f0c0058 # @layout/lb_picker_item 'res/layout/lb_picker_item.xml'

.field public static final lb_picker_separator:I = 0x7f0c0059 # @layout/lb_picker_separator 'res/layout/lb_picker_separator.xml'

.field public static final lb_playback_now_playing_bars:I = 0x7f0c005d # @layout/lb_playback_now_playing_bars 'res/layout/lb_playback_now_playing_bars.xml'

.field public static final lb_search_bar:I = 0x7f0c0065 # @layout/lb_search_bar 'res/layout/lb_search_bar.xml'

.field public static final lb_search_orb:I = 0x7f0c0067 # @layout/lb_search_orb 'res/layout/lb_search_orb.xml'

.field public static final lb_speech_orb:I = 0x7f0c006a # @layout/lb_speech_orb 'res/layout/lb_speech_orb.xml'

.field public static final lb_title_view:I = 0x7f0c006b # @layout/lb_title_view 'res/layout/lb_title_view.xml'
