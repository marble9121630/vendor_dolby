.class public final Landroid/support/design/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final design_bottom_sheet_peek_height_min:I = 0x7f060064 # @dimen/design_bottom_sheet_peek_height_min '64.0dp'

.field public static final design_fab_size_mini:I = 0x7f060068 # @dimen/design_fab_size_mini '40.0dp'

.field public static final design_fab_size_normal:I = 0x7f060069 # @dimen/design_fab_size_normal '56.0dp'

.field public static final design_navigation_icon_size:I = 0x7f06006f # @dimen/design_navigation_icon_size '48.0dp'

.field public static final design_navigation_separator_vertical_padding:I = 0x7f060074 # @dimen/design_navigation_separator_vertical_padding '8.0dp'

.field public static final design_snackbar_padding_vertical:I = 0x7f06007c # @dimen/design_snackbar_padding_vertical '14.0dp'

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f06007d # @dimen/design_snackbar_padding_vertical_2lines '24.0dp'

.field public static final design_tab_scrollable_min_width:I = 0x7f060080 # @dimen/design_tab_scrollable_min_width '72.0dp'

.field public static final design_tab_text_size_2line:I = 0x7f060082 # @dimen/design_tab_text_size_2line '12.0sp'
