.class Lcom/dolby/daxappui/MainActivity$3;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dolby/daxappui/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dolby/daxappui/MainActivity;

.field final synthetic val$mainLayout:Landroid/widget/LinearLayout;

.field final synthetic val$navigationView:Landroid/support/design/widget/NavigationView;


# direct methods
.method constructor <init>(Lcom/dolby/daxappui/MainActivity;Landroid/widget/LinearLayout;Landroid/support/design/widget/NavigationView;)V
    .locals 0

    .line 465
    iput-object p1, p0, Lcom/dolby/daxappui/MainActivity$3;->this$0:Lcom/dolby/daxappui/MainActivity;

    iput-object p2, p0, Lcom/dolby/daxappui/MainActivity$3;->val$mainLayout:Landroid/widget/LinearLayout;

    iput-object p3, p0, Lcom/dolby/daxappui/MainActivity$3;->val$navigationView:Landroid/support/design/widget/NavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .line 468
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity$3;->val$mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 469
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity$3;->this$0:Lcom/dolby/daxappui/MainActivity;

    const v1, 0x7f090085    # @id/dsLogoText

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 470
    iget-object v1, p0, Lcom/dolby/daxappui/MainActivity$3;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {v1}, Lcom/dolby/daxappui/MainActivity;->access$600(Lcom/dolby/daxappui/MainActivity;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v3, "DS1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 471
    iget-object v1, p0, Lcom/dolby/daxappui/MainActivity$3;->val$navigationView:Landroid/support/design/widget/NavigationView;

    invoke-virtual {v1}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v3, 0x7f090126    # @id/nav_exploredolby

    .line 472
    invoke-interface {v1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 473
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity$3;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v1, 0x7f100029    # @string/app_name_ds1 'Dolby Audio'

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 475
    :cond_0
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity$3;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v1, 0x7f10002a    # @string/app_title 'Dolby Atmos'

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
