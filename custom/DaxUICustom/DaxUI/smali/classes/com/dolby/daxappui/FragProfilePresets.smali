.class public Lcom/dolby/daxappui/FragProfilePresets;
.super Landroid/support/v4/app/Fragment;
.source "FragProfilePresets.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

.field private mProfileAdapter:Lcom/dolby/daxappui/FragProfilePanelPageAdapter;

.field private mTabletLayout:Z

.field private mTabletProfilesAdapter:Lcom/dolby/daxappui/TabletProfilesAdapter;

.field private mViewPager:Lcom/dolby/daxappui/CustomViewPager;

.field private pagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    .line 89
    new-instance v0, Lcom/dolby/daxappui/FragProfilePresets$1;

    invoke-direct {v0, p0}, Lcom/dolby/daxappui/FragProfilePresets$1;-><init>(Lcom/dolby/daxappui/FragProfilePresets;)V

    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->pagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/dolby/daxappui/FragProfilePresets;)Lcom/dolby/daxappui/IDsFragObserver;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    return-object p0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .line 39
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 42
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/dolby/daxappui/IDsFragObserver;

    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 44
    :catch_0
    new-instance p0, Ljava/lang/ClassCastException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement IDsFragObserver"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 57
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p3

    iput-boolean p3, p0, Lcom/dolby/daxappui/FragProfilePresets;->mTabletLayout:Z

    const/4 p3, 0x0

    const v0, 0x7f0c0031    # @layout/fragprofilepresets 'res/layout/fragprofilepresets.xml'

    .line 58
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 60
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object p2

    invoke-virtual {p2}, Lcom/dolby/daxappui/DAXApplication;->getProfileNames()[Ljava/lang/String;

    move-result-object p2

    .line 62
    iget-boolean v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mTabletLayout:Z

    if-eqz v0, :cond_0

    const p2, 0x7f090148    # @id/presetsListView

    .line 63
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/AdapterView;

    .line 64
    new-instance p3, Lcom/dolby/daxappui/TabletProfilesAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p3, v0}, Lcom/dolby/daxappui/TabletProfilesAdapter;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/dolby/daxappui/FragProfilePresets;->mTabletProfilesAdapter:Lcom/dolby/daxappui/TabletProfilesAdapter;

    if-eqz p2, :cond_2

    .line 66
    iget-object p3, p0, Lcom/dolby/daxappui/FragProfilePresets;->mTabletProfilesAdapter:Lcom/dolby/daxappui/TabletProfilesAdapter;

    invoke-virtual {p2, p3}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 67
    invoke-virtual {p2, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_1

    :cond_0
    const v0, 0x7f090152    # @id/profiletable

    .line 70
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    .line 71
    array-length v1, p2

    move v2, p3

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p2, v2

    .line 72
    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/support/design/widget/TabLayout$Tab;->setText(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    invoke-virtual {v0, v4}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    const p2, 0x7f090151    # @id/profileViewpager

    .line 75
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/dolby/daxappui/CustomViewPager;

    iput-object p2, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    .line 76
    iget-object p2, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    invoke-virtual {p2, p3}, Lcom/dolby/daxappui/CustomViewPager;->setPagingEnabled(Z)V

    .line 77
    new-instance p2, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object p2, p0, Lcom/dolby/daxappui/FragProfilePresets;->mProfileAdapter:Lcom/dolby/daxappui/FragProfilePanelPageAdapter;

    .line 79
    iget-object p2, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    iget-object p3, p0, Lcom/dolby/daxappui/FragProfilePresets;->mProfileAdapter:Lcom/dolby/daxappui/FragProfilePanelPageAdapter;

    invoke-virtual {p2, p3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 80
    iget-object p2, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    iget-object p3, p0, Lcom/dolby/daxappui/FragProfilePresets;->pagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {p2, p3}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 81
    iget-object p2, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    const/4 p3, 0x3

    invoke-virtual {p2, p3}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 82
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    invoke-virtual {v0, p0}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    :cond_2
    :goto_1
    return-object p1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 141
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/dolby/daxappui/FragProfilePresets;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    if-nez p2, :cond_0

    goto :goto_0

    .line 144
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object p2

    const p4, 0x7f090148    # @id/presetsListView

    invoke-virtual {p2, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-ne p1, p2, :cond_1

    .line 145
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-interface {p0, p3}, Lcom/dolby/daxappui/IDsFragObserver;->chooseProfile(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setGeqViewEnabled(I)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mProfileAdapter:Lcom/dolby/daxappui/FragProfilePanelPageAdapter;

    if-eqz v0, :cond_0

    .line 134
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/dolby/daxappui/FragProfilePanel;

    .line 135
    invoke-virtual {p0}, Lcom/dolby/daxappui/FragProfilePanel;->setGeqViewEnabled()V

    :cond_0
    return-void
.end method

.method public updateProfileSettings(I)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_4

    .line 105
    iget-boolean v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mTabletLayout:Z

    if-eqz v0, :cond_1

    .line 106
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mTabletProfilesAdapter:Lcom/dolby/daxappui/TabletProfilesAdapter;

    if-eqz p0, :cond_3

    .line 107
    invoke-virtual {p0, p1}, Lcom/dolby/daxappui/TabletProfilesAdapter;->setProfileSelected(I)V

    goto :goto_1

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    if-eqz v0, :cond_3

    .line 111
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    const v1, 0x7f090152    # @id/profiletable

    .line 113
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    .line 114
    invoke-virtual {v0, p1}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 116
    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout$Tab;->select()V

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 120
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mProfileAdapter:Lcom/dolby/daxappui/FragProfilePanelPageAdapter;

    if-eqz v0, :cond_3

    .line 121
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePresets;->mViewPager:Lcom/dolby/daxappui/CustomViewPager;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/dolby/daxappui/FragProfilePanel;

    .line 122
    invoke-virtual {p0, p1}, Lcom/dolby/daxappui/FragProfilePanel;->updateProfilePanel(I)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    const-string p0, "FragProfilePresets"

    const-string p1, "updateProfileSettings(): Dolby audio effect is null!"

    .line 127
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
