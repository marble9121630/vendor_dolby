.class public Lcom/dolby/daxappui/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field static final DEFAULT_BLUETOOTH_PORT:I

.field static final DEFAULT_HDMI_PORT:I

.field static final DEFAULT_HEADPHONE_PORT:I

.field static final DEFAULT_MIRACAST_PORT:I

.field static final DEFAULT_SPEAKER_PORT:I

.field static final DEFAULT_USB_PORT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 47
    sget-object v0, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    sput v0, Lcom/dolby/daxappui/Constants;->DEFAULT_SPEAKER_PORT:I

    .line 48
    sget-object v0, Lcom/dolby/dax/DsTuning;->hdmi:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    sput v0, Lcom/dolby/daxappui/Constants;->DEFAULT_HDMI_PORT:I

    .line 49
    sget-object v0, Lcom/dolby/dax/DsTuning;->miracast:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    sput v0, Lcom/dolby/daxappui/Constants;->DEFAULT_MIRACAST_PORT:I

    .line 50
    sget-object v0, Lcom/dolby/dax/DsTuning;->headphone:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    sput v0, Lcom/dolby/daxappui/Constants;->DEFAULT_HEADPHONE_PORT:I

    .line 51
    sget-object v0, Lcom/dolby/dax/DsTuning;->bluetooth:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    sput v0, Lcom/dolby/daxappui/Constants;->DEFAULT_BLUETOOTH_PORT:I

    .line 52
    sget-object v0, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    sput v0, Lcom/dolby/daxappui/Constants;->DEFAULT_USB_PORT:I

    return-void
.end method
