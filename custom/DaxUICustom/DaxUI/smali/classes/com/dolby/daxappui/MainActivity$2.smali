.class Lcom/dolby/daxappui/MainActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dolby/daxappui/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dolby/daxappui/MainActivity;


# direct methods
.method constructor <init>(Lcom/dolby/daxappui/MainActivity;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .line 194
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    .line 195
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDeviceReceiver, action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MainActivity"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "android.intent.action.HEADSET_PLUG"

    .line 200
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "state"

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    .line 202
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_d

    .line 205
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    const/16 p2, 0x8

    invoke-static {p1, p2}, Lcom/dolby/daxappui/MainActivity;->access$202(Lcom/dolby/daxappui/MainActivity;I)I

    goto/16 :goto_3

    :cond_0
    const-string v0, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    .line 209
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string p1, "device"

    .line 210
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/hardware/usb/UsbDevice;

    if-eqz p1, :cond_8

    move p2, v3

    .line 212
    :goto_0
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 213
    invoke-virtual {p1, v3}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 216
    iget-object p2, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    const/16 v0, 0x4000

    invoke-static {p2, v0}, Lcom/dolby/daxappui/MainActivity;->access$202(Lcom/dolby/daxappui/MainActivity;I)I

    move p2, v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v3, p2

    goto/16 :goto_1

    :cond_3
    const-string v0, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    .line 220
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-wide/16 v5, 0x3e8

    if-eqz v0, :cond_4

    .line 221
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$300(Lcom/dolby/daxappui/MainActivity;)I

    move-result p1

    const p2, 0x4004000

    and-int/2addr p1, p2

    if-eqz p1, :cond_8

    .line 225
    :try_start_0
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_4

    :catch_0
    move-exception p1

    .line 227
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    :cond_4
    const-string v0, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    .line 230
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/high16 p1, -0x80000000

    const-string v0, "android.bluetooth.profile.extra.STATE"

    .line 231
    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    const/4 p2, 0x2

    if-ne p1, p2, :cond_5

    .line 234
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    const/16 p2, 0x80

    invoke-static {p1, p2}, Lcom/dolby/daxappui/MainActivity;->access$202(Lcom/dolby/daxappui/MainActivity;I)I

    goto/16 :goto_3

    :cond_5
    if-nez p1, :cond_8

    .line 236
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$300(Lcom/dolby/daxappui/MainActivity;)I

    move-result p1

    and-int/lit16 p1, p1, 0x380

    if-eqz p1, :cond_8

    const-wide/16 p1, 0xc8

    .line 240
    :try_start_1
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_4

    :catch_1
    move-exception p1

    .line 242
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    :cond_6
    const-string v0, "android.bluetooth.adapter.action.STATE_CHANGED"

    .line 246
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 p1, -0x1

    const-string v0, "android.bluetooth.adapter.extra.STATE"

    .line 248
    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    const/16 p2, 0xa

    if-ne p1, p2, :cond_8

    .line 250
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$300(Lcom/dolby/daxappui/MainActivity;)I

    move-result p1

    and-int/lit16 p1, p1, 0x380

    if-eqz p1, :cond_8

    .line 254
    :try_start_2
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_4

    :catch_2
    move-exception p1

    .line 256
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    :cond_7
    const-string v0, "android.intent.action.HDMI_PLUGGED"

    .line 260
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "android.media.action.HDMI_AUDIO_PLUG"

    .line 261
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    goto :goto_2

    :cond_8
    :goto_1
    move v7, v4

    move v4, v3

    move v3, v7

    goto :goto_4

    .line 263
    :cond_9
    :goto_2
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_b

    .line 266
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$400(Lcom/dolby/daxappui/MainActivity;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object p1

    if-eqz p1, :cond_c

    .line 267
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$500(Lcom/dolby/daxappui/MainActivity;)Landroid/widget/RelativeLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_a

    .line 269
    iget-object p2, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p2}, Lcom/dolby/daxappui/MainActivity;->access$500(Lcom/dolby/daxappui/MainActivity;)Landroid/widget/RelativeLayout;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 271
    :cond_a
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$400(Lcom/dolby/daxappui/MainActivity;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object p1

    iget-object p2, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p2}, Lcom/dolby/daxappui/MainActivity;->access$500(Lcom/dolby/daxappui/MainActivity;)Landroid/widget/RelativeLayout;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_3

    .line 274
    :cond_b
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$500(Lcom/dolby/daxappui/MainActivity;)Landroid/widget/RelativeLayout;

    move-result-object p1

    if-eqz p1, :cond_c

    .line 275
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$500(Lcom/dolby/daxappui/MainActivity;)Landroid/widget/RelativeLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_c

    .line 277
    iget-object p2, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p2}, Lcom/dolby/daxappui/MainActivity;->access$500(Lcom/dolby/daxappui/MainActivity;)Landroid/widget/RelativeLayout;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_c
    :goto_3
    move v3, v4

    :cond_d
    :goto_4
    if-eqz v4, :cond_f

    if-nez v3, :cond_e

    .line 286
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p1}, Lcom/dolby/daxappui/MainActivity;->access$300(Lcom/dolby/daxappui/MainActivity;)I

    move-result p2

    invoke-static {p1, p2}, Lcom/dolby/daxappui/MainActivity;->access$202(Lcom/dolby/daxappui/MainActivity;I)I

    .line 288
    :cond_e
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity$2;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-virtual {p0}, Lcom/dolby/daxappui/MainActivity;->setGeqViewEnabled()V

    :cond_f
    return-void
.end method
