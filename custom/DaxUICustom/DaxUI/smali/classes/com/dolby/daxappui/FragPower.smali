.class public Lcom/dolby/daxappui/FragPower;
.super Landroid/support/v4/app/Fragment;
.source "FragPower.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private imgOn:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mFObserver:Lcom/dolby/daxappui/IDsFragObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/dolby/daxappui/FragPower;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .line 32
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 35
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/dolby/daxappui/IDsFragObserver;

    iput-object v0, p0, Lcom/dolby/daxappui/FragPower;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    iput-object p1, p0, Lcom/dolby/daxappui/FragPower;->mContext:Landroid/content/Context;

    return-void

    .line 37
    :catch_0
    new-instance p0, Ljava/lang/ClassCastException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement IDsFragObserver"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    .line 79
    iget-object p0, p0, Lcom/dolby/daxappui/FragPower;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    :goto_0
    const-string v0, "FragPower"

    if-nez p0, :cond_1

    const-string p0, "onClick(): Dolby audio effect is null!"

    .line 81
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const v1, 0x7f090143    # @id/powerButtonOn

    if-ne v1, p1, :cond_3

    .line 85
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getDsOn()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 86
    invoke-virtual {p0}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 87
    invoke-virtual {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->setDsOn(Z)V

    goto :goto_1

    :cond_2
    const-string p0, "Dolby audio effect is out of control in FragPower"

    .line 89
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 49
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 50
    iget-object p0, p0, Lcom/dolby/daxappui/FragPower;->imgOn:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setLayoutDirection(I)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 44
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const/4 p3, 0x0

    const v0, 0x7f0c002f    # @layout/fragpower 'res/layout/fragpower.xml'

    .line 57
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f090143    # @id/powerButtonOn

    .line 58
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/dolby/daxappui/FragPower;->imgOn:Landroid/widget/ImageView;

    .line 59
    iget-object p2, p0, Lcom/dolby/daxappui/FragPower;->imgOn:Landroid/widget/ImageView;

    invoke-virtual {p2, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iget-object p2, p0, Lcom/dolby/daxappui/FragPower;->imgOn:Landroid/widget/ImageView;

    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 62
    iget-object p2, p0, Lcom/dolby/daxappui/FragPower;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p2

    :goto_0
    if-eqz p2, :cond_1

    .line 64
    invoke-virtual {p2}, Lcom/dolby/dax/DolbyAudioEffect;->getDsOn()Z

    move-result p2

    goto :goto_1

    :cond_1
    const-string p2, "FragPower"

    const-string p3, "onCreateView(): Dolby audio effect is null!"

    .line 66
    invoke-static {p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_2

    .line 69
    iget-object p2, p0, Lcom/dolby/daxappui/FragPower;->imgOn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f070063    # @drawable/btn_power_on_titlebar 'res/drawable/btn_power_on_titlebar.xml'

    iget-object p0, p0, Lcom/dolby/daxappui/FragPower;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    invoke-virtual {p3, v0, p0}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p2, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 71
    :cond_2
    iget-object p2, p0, Lcom/dolby/daxappui/FragPower;->imgOn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f070062    # @drawable/btn_power_off_titlebar 'res/drawable/btn_power_off_titlebar.xml'

    iget-object p0, p0, Lcom/dolby/daxappui/FragPower;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    invoke-virtual {p3, v0, p0}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p2, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    return-object p1
.end method
