.class public Lcom/dolby/daxappui/reset/FragReset;
.super Landroid/support/v4/app/Fragment;
.source "FragReset.java"


# instance fields
.field private mFObserver:Lcom/dolby/daxappui/IDsFragObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onCreateView$0$FragReset(Landroid/view/View;)V
    .locals 0

    .line 50
    sget-object p1, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object p0, p0, Lcom/dolby/daxappui/reset/FragReset;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {p1, p0}, Lcom/dolby/daxappui/DsClientSettings;->resetAllProfileSpecificSettings(Lcom/dolby/daxappui/IDsFragObserver;)V

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .line 32
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 35
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/dolby/daxappui/IDsFragObserver;

    iput-object v0, p0, Lcom/dolby/daxappui/reset/FragReset;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 37
    :catch_0
    new-instance p0, Ljava/lang/ClassCastException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement IDsFragObserver"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 44
    invoke-super {p0, p3}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const p3, 0x7f0c0084    # @layout/reset 'res/layout/reset.xml'

    const/4 v0, 0x0

    .line 46
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f090156    # @id/resetButtonView

    .line 48
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/RelativeLayout;

    .line 49
    new-instance p3, Lcom/dolby/daxappui/reset/-$$Lambda$FragReset$kGyPNc8iWrVBFBFKhmrGwmWzAMU;

    invoke-direct {p3, p0}, Lcom/dolby/daxappui/reset/-$$Lambda$FragReset$kGyPNc8iWrVBFBFKhmrGwmWzAMU;-><init>(Lcom/dolby/daxappui/reset/FragReset;)V

    invoke-virtual {p2, p3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method
