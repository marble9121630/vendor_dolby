.class Lcom/dolby/daxappui/EqualizerAdapter;
.super Landroid/widget/BaseAdapter;
.source "EqualizerAdapter.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;,
        Lcom/dolby/daxappui/EqualizerAdapter$Holder;
    }
.end annotation


# instance fields
.field private imgIdForIeqOff:[I

.field private imgIdForIeqOn:[I

.field private mContext:Landroid/content/Context;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mListener:Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;

.field private mPosition:Ljava/lang/Integer;

.field private mSelectedPreset:I


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;)V
    .locals 2

    .line 57
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, -0x1

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mPosition:Ljava/lang/Integer;

    .line 31
    iput v0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mSelectedPreset:I

    const/4 v0, 0x3

    new-array v1, v0, [I

    .line 33
    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->imgIdForIeqOn:[I

    new-array v0, v0, [I

    .line 39
    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->imgIdForIeqOff:[I

    .line 58
    iput-object p1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mContext:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mListener:Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;

    return-void

    :array_0
    .array-data 4
        0x7f070073    # @drawable/ic_focus_on_ieq 'res/drawable/ic_focus_on_ieq.xml'
        0x7f070080    # @drawable/ic_open_on_ieq 'res/drawable/ic_open_on_ieq.xml'
        0x7f070085    # @drawable/ic_rich_on_ieq 'res/drawable/ic_rich_on_ieq.xml'
    .end array-data

    :array_1
    .array-data 4
        0x7f070072    # @drawable/ic_focus_off_ieq 'res/drawable/ic_focus_off_ieq.xml'
        0x7f07007f    # @drawable/ic_open_off_ieq 'res/drawable/ic_open_off_ieq.xml'
        0x7f070084    # @drawable/ic_rich_off_ieq 'res/drawable/ic_rich_off_ieq.xml'
    .end array-data
.end method

.method static synthetic access$102(Lcom/dolby/daxappui/EqualizerAdapter;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mPosition:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/dolby/daxappui/EqualizerAdapter;)Landroid/view/GestureDetector;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mGestureDetector:Landroid/view/GestureDetector;

    return-object p0
.end method


# virtual methods
.method public getCount()I
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->imgIdForIeqOn:[I

    array-length p0, p0

    return p0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    .line 74
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public getItemId(I)J
    .locals 0

    int-to-long p0, p1

    return-wide p0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    .line 81
    iget-object p2, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const p3, 0x7f0c002d    # @layout/equalizer_list_item 'res/layout/equalizer_list_item.xml'

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 83
    new-instance p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;

    invoke-direct {p3, p0, v0}, Lcom/dolby/daxappui/EqualizerAdapter$Holder;-><init>(Lcom/dolby/daxappui/EqualizerAdapter;Lcom/dolby/daxappui/EqualizerAdapter$1;)V

    const v0, 0x7f0900d0    # @id/icon

    .line 84
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;->imageView:Landroid/widget/ImageView;

    const v0, 0x7f090091    # @id/equalizerListViewLayout

    .line 85
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;->linearLayout:Landroid/widget/LinearLayout;

    .line 86
    iput p1, p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;->position:I

    .line 87
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mGestureDetector:Landroid/view/GestureDetector;

    .line 88
    new-instance v0, Lcom/dolby/daxappui/EqualizerAdapter$1;

    invoke-direct {v0, p0}, Lcom/dolby/daxappui/EqualizerAdapter$1;-><init>(Lcom/dolby/daxappui/EqualizerAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 96
    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;

    .line 101
    :goto_0
    iget v0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mSelectedPreset:I

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    .line 103
    iget-object v0, p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;->linearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f070060    # @drawable/background_with_blue_shadow 'res/drawable/background_with_blue_shadow.xml'

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 104
    iget-object p3, p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;->imageView:Landroid/widget/ImageView;

    iget-object p0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->imgIdForIeqOn:[I

    aget p0, p0, p1

    invoke-virtual {p3, p0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 106
    :cond_2
    iget-object v0, p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;->linearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f070061    # @drawable/background_with_shadow 'res/drawable/background_with_shadow.xml'

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 107
    iget-object p3, p3, Lcom/dolby/daxappui/EqualizerAdapter$Holder;->imageView:Landroid/widget/ImageView;

    iget-object p0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->imgIdForIeqOff:[I

    aget p0, p0, p1

    invoke-virtual {p3, p0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_2
    return-object p2
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 125
    iget-object p1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mPosition:Ljava/lang/Integer;

    if-eqz p1, :cond_0

    .line 126
    iget-object v0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mListener:Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;->onPresetChanged(IZ)V

    .line 127
    iget-object p1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mPosition:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mSelectedPreset:I

    .line 128
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method setIeqSelection(IZ)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mListener:Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;

    invoke-interface {v0, p1, p2}, Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;->onPresetChanged(IZ)V

    .line 150
    iput p1, p0, Lcom/dolby/daxappui/EqualizerAdapter;->mSelectedPreset:I

    .line 151
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
