.class public Lcom/dolby/daxappui/GeqView;
.super Landroid/view/View;
.source "GeqView.java"


# instance fields
.field private mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

.field private mContext:Landroid/content/Context;

.field private mCurrentYPosition:F

.field private mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

.field private mGeqData:[F

.field private mGeqDataTitlePaint:Landroid/graphics/Paint;

.field private mHLinePaint:Landroid/graphics/Paint;

.field private mRecPaint:Landroid/graphics/Paint;

.field private mRect:Landroid/graphics/Rect;

.field public mSelectedBar:I

.field private mStokeLinePath:Landroid/graphics/Path;

.field private mStrokeLinePaint:Landroid/graphics/Paint;

.field private mTitlePaint:Landroid/graphics/Paint;

.field private mUserGain:[I

.field private mXLeftPosition:[I

.field private mXRightPosition:[I

.field private final yTitlesStrings:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 107
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 35
    const-string v0, "+12dB"

    const-string v1, "0dB"

    const-string v2, "-12dB"

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->yTitlesStrings:[Ljava/lang/String;

    .line 43
    const/16 v0, 0xa

    new-array v1, v0, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    .line 44
    const/16 v1, 0x14

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    .line 45
    new-array v1, v0, [I

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/dolby/daxappui/GeqView;->mXLeftPosition:[I

    .line 46
    new-array v0, v0, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mXRightPosition:[I

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    .line 108
    invoke-direct {p0}, Lcom/dolby/daxappui/GeqView;->init()V

    .line 111
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/dolby/daxappui/IDsFragObserver;

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    nop

    .line 115
    iput-object p1, p0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    .line 116
    iget-object p1, p0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/dolby/daxappui/DAXConfiguration;->getInstance(Landroid/content/Context;)Lcom/dolby/daxappui/DAXConfiguration;

    move-result-object p1

    iput-object p1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    .line 117
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement IDsFragObserver"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 120
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const-string p2, "+12dB"

    const-string v0, "0dB"

    const-string v1, "-12dB"

    filled-new-array {p2, v0, v1}, [Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/dolby/daxappui/GeqView;->yTitlesStrings:[Ljava/lang/String;

    .line 43
    const/16 p2, 0xa

    new-array v0, p2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    .line 44
    const/16 v0, 0x14

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    .line 45
    new-array v0, p2, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mXLeftPosition:[I

    .line 46
    new-array p2, p2, [I

    fill-array-data p2, :array_2

    iput-object p2, p0, Lcom/dolby/daxappui/GeqView;->mXRightPosition:[I

    .line 50
    const/4 p2, -0x1

    iput p2, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    .line 121
    invoke-direct {p0}, Lcom/dolby/daxappui/GeqView;->init()V

    .line 124
    :try_start_0
    move-object p2, p1

    check-cast p2, Lcom/dolby/daxappui/IDsFragObserver;

    iput-object p2, p0, Lcom/dolby/daxappui/GeqView;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    nop

    .line 128
    iput-object p1, p0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    .line 129
    iget-object p1, p0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/dolby/daxappui/DAXConfiguration;->getInstance(Landroid/content/Context;)Lcom/dolby/daxappui/DAXConfiguration;

    move-result-object p1

    iput-object p1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    .line 130
    return-void

    .line 125
    :catch_0
    move-exception p2

    .line 126
    new-instance p2, Ljava/lang/ClassCastException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement IDsFragObserver"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw p2

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private init()V
    .locals 4

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    .line 55
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 56
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    .line 57
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mHLinePaint:Landroid/graphics/Paint;

    .line 58
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mStrokeLinePaint:Landroid/graphics/Paint;

    .line 59
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x4

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 60
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mStrokeLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 61
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mStokeLinePath:Landroid/graphics/Path;

    .line 62
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqDataTitlePaint:Landroid/graphics/Paint;

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    .line 65
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    invoke-virtual {p0, v0}, Lcom/dolby/daxappui/GeqView;->updateThisData([F)V

    .line 66
    return-void

    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private updateUserGain()V
    .locals 7

    .line 69
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    if-nez v0, :cond_0

    .line 70
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v0, v0, v1

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v1}, Lcom/dolby/daxappui/DAXConfiguration;->getMinEditGain()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v0, v0, v1

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v1}, Lcom/dolby/daxappui/DAXConfiguration;->getMaxEditGain()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    .line 74
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v0, v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 75
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v2, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v1, v1, v2

    sub-float/2addr v1, v0

    const/high16 v2, 0x3e800000    # 0.25f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 76
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v2, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aput v0, v1, v2

    goto :goto_0

    .line 77
    :cond_1
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v1, v1, v3

    sub-float/2addr v1, v0

    cmpl-float v1, v1, v2

    const/high16 v2, 0x3f400000    # 0.75f

    if-ltz v1, :cond_2

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v1, v1, v3

    sub-float/2addr v1, v0

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_2

    .line 78
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v2, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v0, v3

    aput v0, v1, v2

    goto :goto_0

    .line 79
    :cond_2
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v1, v1, v3

    sub-float/2addr v1, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 80
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v2, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v0, v3

    aput v0, v1, v2

    .line 84
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v0, v0, v1

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v1}, Lcom/dolby/daxappui/DAXConfiguration;->getMinEditGain()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 85
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v2}, Lcom/dolby/daxappui/DAXConfiguration;->getMinEditGain()F

    move-result v2

    aput v2, v0, v1

    goto :goto_1

    .line 86
    :cond_4
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v0, v0, v1

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v1}, Lcom/dolby/daxappui/DAXConfiguration;->getMaxEditGain()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 87
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v2}, Lcom/dolby/daxappui/DAXConfiguration;->getMaxEditGain()F

    move-result v2

    aput v2, v0, v1

    .line 90
    :cond_5
    :goto_1
    iget v0, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const/high16 v1, 0x40000000    # 2.0f

    const/high16 v2, 0x41800000    # 16.0f

    if-nez v0, :cond_6

    .line 91
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    iget-object v4, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v5, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v4, v4, v5

    mul-float/2addr v4, v2

    float-to-int v4, v4

    aput v4, v0, v3

    .line 92
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v5, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v4, v4, v5

    iget-object v5, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v6, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    add-int/lit8 v6, v6, 0x1

    aget v5, v5, v6

    add-float/2addr v4, v5

    div-float/2addr v4, v1

    mul-float/2addr v4, v2

    float-to-int v1, v4

    aput v1, v0, v3

    goto/16 :goto_2

    .line 93
    :cond_6
    iget v0, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_7

    .line 94
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v5, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v6, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v5, v5, v6

    add-float/2addr v4, v5

    div-float/2addr v4, v1

    mul-float/2addr v4, v2

    float-to-int v1, v4

    aput v1, v0, v3

    .line 95
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    mul-int/lit8 v1, v1, 0x2

    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v4, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v3, v3, v4

    mul-float/2addr v3, v2

    float-to-int v3, v3

    aput v3, v0, v1

    .line 96
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget v1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v4, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v3, v3, v4

    mul-float/2addr v3, v2

    float-to-int v2, v3

    aput v2, v0, v1

    goto :goto_2

    .line 98
    :cond_7
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v5, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v6, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v5, v5, v6

    add-float/2addr v4, v5

    div-float/2addr v4, v1

    mul-float/2addr v4, v2

    float-to-int v4, v4

    aput v4, v0, v3

    .line 99
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    mul-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v5, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v4, v4, v5

    mul-float/2addr v4, v2

    float-to-int v4, v4

    aput v4, v0, v3

    .line 100
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v5, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v4, v4, v5

    iget-object v5, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v6, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    add-int/lit8 v6, v6, 0x1

    aget v5, v5, v6

    add-float/2addr v4, v5

    div-float/2addr v4, v1

    mul-float/2addr v4, v2

    float-to-int v1, v4

    aput v1, v0, v3

    .line 103
    :goto_2
    sget-object v0, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    invoke-virtual {v0, v1, v2}, Lcom/dolby/daxappui/DsClientSettings;->setGraphicEqualizerBandGains(Lcom/dolby/daxappui/IDsFragObserver;[I)V

    .line 104
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 27

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    .line 212
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 214
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 215
    sget-object v2, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-interface {v3}, Lcom/dolby/daxappui/IDsFragObserver;->getActivePort()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dolby/daxappui/DsClientSettings;->getGraphicEqualizerOn(Lcom/dolby/daxappui/IDsFragObserver;I)Z

    move-result v8

    .line 216
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v9, v2, Landroid/util/DisplayMetrics;->density:F

    .line 217
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v10, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const/4 v11, 0x2

    mul-int/2addr v3, v11

    sub-int v12, v2, v3

    if-eqz v1, :cond_0

    .line 220
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060089    # @dimen/geq_bar_end_padding '24.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0

    .line 222
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v2

    :goto_0
    int-to-float v3, v12

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v3, v13

    .line 226
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_1

    .line 229
    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_1

    .line 231
    :cond_1
    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 234
    :goto_1
    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f06008d    # @dimen/geq_text_size '11.0sp'

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    const v5, 0x7f050033    # @color/colorGeqDisableText '@android:color/primary_text_light'

    if-eqz v8, :cond_2

    .line 236
    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v11, 0x7f050034    # @color/colorGeqGainText '#94000000'

    iget-object v13, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v13

    invoke-virtual {v6, v11, v13}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_2

    .line 238
    :cond_2
    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v11, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v11

    invoke-virtual {v6, v5, v11}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 240
    :goto_2
    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v4

    .line 241
    iget v6, v4, Landroid/graphics/Paint$FontMetrics;->descent:F

    float-to-int v6, v6

    .line 242
    iget v11, v4, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v4, v4, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v11, v4

    .line 243
    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    iget-object v13, v0, Lcom/dolby/daxappui/GeqView;->yTitlesStrings:[Ljava/lang/String;

    const/16 v19, 0x0

    aget-object v13, v13, v19

    invoke-virtual {v4, v13}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    if-ne v14, v15, :cond_3

    .line 246
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v13

    int-to-float v13, v13

    sub-float v4, v13, v4

    :cond_3
    move/from16 v13, v19

    .line 248
    :goto_3
    iget-object v5, v0, Lcom/dolby/daxappui/GeqView;->yTitlesStrings:[Ljava/lang/String;

    array-length v5, v5

    if-ge v13, v5, :cond_6

    if-nez v13, :cond_4

    int-to-float v5, v6

    sub-float v5, v11, v5

    .line 252
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    invoke-virtual {v15, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    int-to-float v15, v15

    add-float/2addr v5, v15

    goto :goto_5

    :cond_4
    move v5, v15

    if-ne v13, v5, :cond_5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v15, v11, v5

    add-float/2addr v15, v3

    int-to-float v5, v6

    sub-float/2addr v15, v5

    .line 254
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    goto :goto_4

    :cond_5
    const/high16 v5, 0x40000000    # 2.0f

    mul-float v15, v3, v5

    int-to-float v5, v6

    sub-float/2addr v15, v5

    .line 256
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    :goto_4
    int-to-float v5, v5

    add-float/2addr v5, v15

    .line 258
    :goto_5
    iget-object v15, v0, Lcom/dolby/daxappui/GeqView;->yTitlesStrings:[Ljava/lang/String;

    aget-object v15, v15, v13

    iget-object v10, v0, Lcom/dolby/daxappui/GeqView;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v15, v4, v5, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v13, v13, 0x1

    const v10, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    const/4 v15, 0x1

    goto :goto_3

    .line 262
    :cond_6
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06008c    # @dimen/geq_text_and_bar_margin '50.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 263
    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    array-length v3, v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-int v10, v2

    if-eqz v1, :cond_7

    int-to-float v1, v10

    const v2, 0x3f59999a    # 0.85f

    mul-float/2addr v2, v1

    const v3, 0x3e19999a    # 0.15f

    goto :goto_6

    :cond_7
    int-to-float v1, v10

    const v2, 0x3f6b851f    # 0.92f

    mul-float/2addr v2, v1

    const v3, 0x3da3d70a    # 0.08f

    :goto_6
    mul-float/2addr v1, v3

    move v13, v1

    move v11, v2

    .line 273
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    const/4 v2, 0x1

    if-ne v14, v2, :cond_8

    .line 275
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    :cond_8
    move v15, v1

    move/from16 v1, v19

    .line 277
    :goto_7
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 278
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050030    # @color/colorGeqBackgroundBar '#12000000'

    iget-object v5, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 279
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v2}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    const/4 v2, 0x1

    if-ne v14, v2, :cond_a

    .line 281
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v10

    sub-int v4, v15, v3

    iput v4, v2, Landroid/graphics/Rect;->left:I

    if-nez v1, :cond_9

    int-to-float v3, v15

    sub-float/2addr v3, v13

    float-to-int v3, v3

    .line 283
    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto :goto_9

    :cond_9
    int-to-float v3, v3

    sub-float/2addr v3, v11

    float-to-int v3, v3

    sub-int v3, v15, v3

    .line 285
    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto :goto_9

    :cond_a
    if-nez v1, :cond_b

    .line 289
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    int-to-float v3, v15

    add-float/2addr v3, v13

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto :goto_8

    .line 291
    :cond_b
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v10

    int-to-float v3, v3

    sub-float/2addr v3, v11

    float-to-int v3, v3

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 293
    :goto_8
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v10

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 295
    :goto_9
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 296
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v3, v12

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 298
    iget v2, v0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    if-eq v2, v1, :cond_c

    .line 299
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_7

    .line 304
    :cond_d
    iget v1, v0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const/4 v2, -0x1

    const/high16 v23, 0x41400000    # 12.0f

    if-eq v1, v2, :cond_f

    .line 305
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05004e    # @color/colorSelectedGeqBackgroundBar '@android:color/system_accent1_100'

    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 306
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v1}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 307
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    div-int/lit8 v2, v12, 0x2

    add-int/2addr v1, v2

    int-to-float v2, v2

    .line 308
    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget v4, v0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    aget v3, v3, v4

    div-float v3, v3, v23

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 310
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 311
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->bottom:I

    const/4 v1, 0x1

    if-ne v14, v1, :cond_e

    .line 314
    iget v3, v0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    mul-int v4, v10, v3

    sub-int v4, v15, v4

    iput v4, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v1

    mul-int/2addr v3, v10

    int-to-float v1, v3

    add-float/2addr v1, v13

    float-to-int v1, v1

    sub-int v1, v15, v1

    .line 315
    iput v1, v2, Landroid/graphics/Rect;->left:I

    goto :goto_a

    .line 317
    :cond_e
    iget v1, v0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    mul-int v3, v10, v1

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->left:I

    const/4 v3, 0x1

    add-int/2addr v1, v3

    mul-int/2addr v1, v10

    int-to-float v1, v1

    add-float/2addr v1, v13

    float-to-int v1, v1

    add-int/2addr v1, v15

    .line 318
    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 321
    :goto_a
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_f
    move/from16 v6, v19

    .line 324
    :goto_b
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    array-length v2, v1

    if-ge v6, v2, :cond_1a

    .line 325
    aget v1, v1, v6

    .line 327
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v2}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 328
    iget v2, v0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const v5, 0x7f05004f    # @color/colorSelectedGeqBar '@android:color/system_accent1_600'

    if-ne v2, v6, :cond_10

    .line 329
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Lcom/dolby/daxappui/GeqView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_c

    .line 331
    :cond_10
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Lcom/dolby/daxappui/GeqView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050031    # @color/colorGeqBar '@android:color/system_accent1_600'

    iget-object v5, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 334
    :goto_c
    const/4 v2, 0x1

    if-ne v14, v2, :cond_12

    if-nez v6, :cond_11

    .line 342
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    int-to-float v3, v15

    sub-float/2addr v3, v13

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto :goto_d

    .line 344
    :cond_11
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v6, 0x1

    mul-int/2addr v3, v10

    int-to-float v3, v3

    sub-float/2addr v3, v11

    float-to-int v3, v3

    sub-int v3, v15, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 346
    :goto_d
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v6, 0x1

    mul-int/2addr v3, v10

    sub-int v3, v15, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto :goto_f

    :cond_12
    if-nez v6, :cond_13

    .line 349
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    int-to-float v3, v15

    add-float/2addr v3, v13

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto :goto_e

    .line 351
    :cond_13
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v6, 0x1

    mul-int/2addr v3, v10

    int-to-float v3, v3

    sub-float/2addr v3, v11

    float-to-int v3, v3

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 353
    :goto_e
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v6, 0x1

    mul-int/2addr v3, v10

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 355
    :goto_f
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050031    # @color/colorGeqBar '@android:color/system_accent1_600'

    iget-object v5, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 359
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mXLeftPosition:[I

    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    aput v4, v2, v6

    .line 360
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mXRightPosition:[I

    iget v3, v3, Landroid/graphics/Rect;->right:I

    aput v3, v2, v6

    .line 362
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    div-int/lit8 v3, v12, 0x2

    add-int/2addr v2, v3

    .line 363
    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    int-to-float v3, v3

    div-float v1, v1, v23

    mul-float/2addr v3, v1

    float-to-int v1, v3

    sub-int/2addr v2, v1

    iput v2, v4, Landroid/graphics/Rect;->top:I

    .line 364
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v12

    iput v1, v4, Landroid/graphics/Rect;->bottom:I

    if-eqz v8, :cond_14

    .line 367
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 370
    :cond_14
    iget v1, v0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const v2, 0x7f06008f    # @dimen/geq_top_line_margin '2.0dp'

    if-ne v1, v6, :cond_19

    if-eqz v8, :cond_15

    .line 372
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mHLinePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050050    # @color/colorSelectedGeqBarTopLine '#ff85b1c6'

    iget-object v5, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    const v5, 0x7f050033    # @color/colorGeqDisableText '@android:color/primary_text_light'

    goto :goto_10

    .line 374
    :cond_15
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mHLinePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const v5, 0x7f050033    # @color/colorGeqDisableText '@android:color/primary_text_light'

    invoke-virtual {v3, v5, v4}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 376
    :goto_10
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mHLinePaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v4, v9, v3

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 377
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v1

    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v4, v1, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    .line 379
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    sub-float v5, v1, v2

    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v1

    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mHLinePaint:Landroid/graphics/Paint;

    move-object/from16 v24, v1

    move-object/from16 v1, p1

    move/from16 v25, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move/from16 v26, v8

    const v8, 0x7f05004f    # @color/colorSelectedGeqBar '@android:color/system_accent1_600'

    const v19, 0x7f050033    # @color/colorGeqDisableText '@android:color/primary_text_light'

    const v20, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    move/from16 v5, v25

    move/from16 v16, v6

    const v8, 0x7f06008d    # @dimen/geq_text_size '11.0sp'

    move-object/from16 v6, v24

    .line 377
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 383
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStrokeLinePaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 384
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStrokeLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 385
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStrokeLinePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05003d    # @color/colorImageOnGeqBarStorke '@android:color/system_accent1_600'

    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 386
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStrokeLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 387
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStokeLinePath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    const/4 v6, 0x1

    if-ne v14, v6, :cond_16

    .line 389
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStokeLinePath:Landroid/graphics/Path;

    sub-int v2, v15, v10

    int-to-float v2, v2

    add-float/2addr v2, v11

    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 390
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStokeLinePath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_11

    .line 392
    :cond_16
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStokeLinePath:Landroid/graphics/Path;

    add-int v2, v15, v10

    int-to-float v2, v2

    sub-float/2addr v2, v11

    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 393
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStokeLinePath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 395
    :goto_11
    iget v1, v0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    if-eqz v1, :cond_17

    .line 396
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mStokeLinePath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mStrokeLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 399
    :cond_17
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008a    # @drawable/image_on_top_geqbar 'res/drawable/image_on_top_geqbar.xml'

    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_18

    .line 401
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06008e    # @dimen/geq_top_image_size '6.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int v3, v10, v3

    float-to-int v5, v13

    add-int/2addr v3, v5

    const/16 v17, 0x2

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 402
    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    add-int/2addr v6, v10

    add-int/2addr v6, v5

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v3, v6

    .line 403
    iget-object v5, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    .line 404
    iget-object v6, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v6, v4

    .line 405
    invoke-virtual {v1, v2, v5, v3, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 406
    invoke-virtual {v1, v7}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 409
    :cond_18
    new-instance v1, Ljava/math/BigDecimal;

    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v2, v2, v16

    float-to-double v2, v2

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    const/4 v2, 0x4

    const/4 v8, 0x2

    .line 410
    invoke-virtual {v1, v8, v2}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F

    move-result v1

    .line 411
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mGeqDataTitlePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f06008d    # @dimen/geq_text_size '11.0sp'

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 412
    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mGeqDataTitlePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const v5, 0x7f05004f    # @color/colorSelectedGeqBar '@android:color/system_accent1_600'

    invoke-virtual {v3, v5, v4}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 413
    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v3

    int-to-float v3, v3

    add-int v5, v15, v10

    int-to-float v5, v5

    sub-float/2addr v5, v11

    sub-float/2addr v3, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    sub-float/2addr v4, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    .line 415
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f06008b    # @dimen/geq_gain_text_and_stroke_line_margin '4.0dp'

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, v0, Lcom/dolby/daxappui/GeqView;->mGeqDataTitlePaint:Landroid/graphics/Paint;

    .line 413
    invoke-virtual {v7, v1, v4, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move/from16 v21, v6

    const/high16 v17, 0x40000000    # 2.0f

    const/16 v22, 0x1

    goto :goto_12

    :cond_19
    move/from16 v20, v5

    move/from16 v16, v6

    move/from16 v26, v8

    const v6, 0x7f06008d    # @dimen/geq_text_size '11.0sp'

    const/4 v8, 0x2

    const v19, 0x7f050033    # @color/colorGeqDisableText '@android:color/primary_text_light'

    .line 418
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mHLinePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050032    # @color/colorGeqBarTopLine '#8cf8fafb'

    iget-object v5, v0, Lcom/dolby/daxappui/GeqView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 419
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mHLinePaint:Landroid/graphics/Paint;

    const/high16 v17, 0x40000000    # 2.0f

    mul-float v3, v9, v17

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 420
    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v1

    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v4, v1, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    .line 422
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    sub-float v5, v1, v2

    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v1

    iget-object v1, v0, Lcom/dolby/daxappui/GeqView;->mHLinePaint:Landroid/graphics/Paint;

    move-object/from16 v18, v1

    move-object/from16 v1, p1

    move/from16 v22, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move/from16 v5, v22

    move/from16 v21, v6

    const/16 v22, 0x1

    move-object/from16 v6, v18

    .line 420
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :goto_12
    add-int/lit8 v6, v16, 0x1

    move/from16 v8, v26

    goto/16 :goto_b

    :cond_1a
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 423
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 424
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .line 139
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 140
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 141
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 142
    invoke-virtual {p0}, Lcom/dolby/daxappui/GeqView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/dolby/daxappui/GeqView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 144
    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    const/4 v4, 0x1

    if-nez v3, :cond_0

    .line 145
    return v4

    .line 148
    :cond_0
    const/4 v3, 0x0

    const/4 v5, -0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_6

    .line 160
    :pswitch_0
    iget v0, p0, Lcom/dolby/daxappui/GeqView;->mCurrentYPosition:F

    sub-float/2addr v0, p1

    .line 161
    :goto_0
    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    array-length v6, v6

    if-ge v3, v6, :cond_8

    .line 162
    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mXLeftPosition:[I

    aget v6, v6, v3

    int-to-float v6, v6

    cmpl-float v6, v1, v6

    if-lez v6, :cond_7

    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mXRightPosition:[I

    aget v6, v6, v3

    int-to-float v6, v6

    cmpg-float v6, v1, v6

    if-gez v6, :cond_7

    .line 164
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    int-to-float v2, v2

    div-float/2addr v0, v2

    const/high16 v2, 0x41c00000    # 24.0f

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v2, v2, v3

    add-float/2addr v0, v2

    aput v0, v1, v3

    .line 165
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v0, v0, v3

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v1}, Lcom/dolby/daxappui/DAXConfiguration;->getMinEditGain()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v1}, Lcom/dolby/daxappui/DAXConfiguration;->getMinEditGain()F

    move-result v1

    aput v1, v0, v3

    goto :goto_1

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v0, v0, v3

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v1}, Lcom/dolby/daxappui/DAXConfiguration;->getMaxEditGain()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v1}, Lcom/dolby/daxappui/DAXConfiguration;->getMaxEditGain()F

    move-result v1

    aput v1, v0, v3

    .line 171
    :cond_2
    :goto_1
    iget v0, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    if-eq v0, v5, :cond_3

    .line 172
    invoke-direct {p0}, Lcom/dolby/daxappui/GeqView;->updateUserGain()V

    .line 173
    invoke-virtual {p0}, Lcom/dolby/daxappui/GeqView;->postInvalidate()V

    .line 176
    :cond_3
    const/high16 v0, 0x40000000    # 2.0f

    const/high16 v1, 0x41800000    # 16.0f

    if-nez v3, :cond_4

    .line 177
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget-object v5, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v5, v5, v3

    mul-float/2addr v5, v1

    float-to-int v5, v5

    aput v5, v2, v3

    .line 178
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    add-int/lit8 v5, v3, 0x1

    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v6, v6, v3

    iget-object v7, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v7, v7, v5

    add-float/2addr v6, v7

    div-float/2addr v6, v0

    mul-float/2addr v6, v1

    float-to-int v0, v6

    aput v0, v2, v5

    goto :goto_2

    .line 179
    :cond_4
    const/16 v2, 0x9

    if-ne v3, v2, :cond_5

    .line 180
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    mul-int/lit8 v5, v3, 0x2

    add-int/lit8 v6, v5, -0x1

    iget-object v7, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    add-int/lit8 v8, v3, -0x1

    aget v7, v7, v8

    iget-object v8, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v8, v8, v3

    add-float/2addr v7, v8

    div-float/2addr v7, v0

    mul-float/2addr v7, v1

    float-to-int v0, v7

    aput v0, v2, v6

    .line 181
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v2, v2, v3

    mul-float/2addr v2, v1

    float-to-int v2, v2

    aput v2, v0, v5

    .line 182
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    add-int/2addr v5, v4

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v2, v2, v3

    mul-float/2addr v2, v1

    float-to-int v1, v2

    aput v1, v0, v5

    goto :goto_2

    .line 183
    :cond_5
    if-lez v3, :cond_6

    if-ge v3, v2, :cond_6

    .line 184
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    mul-int/lit8 v5, v3, 0x2

    add-int/lit8 v6, v5, -0x1

    iget-object v7, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    add-int/lit8 v8, v3, -0x1

    aget v7, v7, v8

    iget-object v8, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v8, v8, v3

    add-float/2addr v7, v8

    div-float/2addr v7, v0

    mul-float/2addr v7, v1

    float-to-int v7, v7

    aput v7, v2, v6

    .line 185
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v6, v6, v3

    mul-float/2addr v6, v1

    float-to-int v6, v6

    aput v6, v2, v5

    .line 186
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    add-int/2addr v5, v4

    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v6, v6, v3

    iget-object v7, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    add-int/lit8 v8, v3, 0x1

    aget v7, v7, v8

    add-float/2addr v6, v7

    div-float/2addr v6, v0

    mul-float/2addr v6, v1

    float-to-int v0, v6

    aput v0, v2, v5

    .line 188
    :cond_6
    :goto_2
    sget-object v0, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    invoke-virtual {v0, v1, v2}, Lcom/dolby/daxappui/DsClientSettings;->setGraphicEqualizerBandGains(Lcom/dolby/daxappui/IDsFragObserver;[I)V

    .line 189
    iput v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    .line 191
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    invoke-virtual {p0, v0}, Lcom/dolby/daxappui/GeqView;->updateThisData([F)V

    .line 192
    iput p1, p0, Lcom/dolby/daxappui/GeqView;->mCurrentYPosition:F

    .line 193
    goto :goto_3

    .line 161
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 197
    :cond_8
    :goto_3
    goto :goto_6

    .line 199
    :pswitch_1
    iget p1, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    if-eq p1, v5, :cond_b

    .line 200
    invoke-direct {p0}, Lcom/dolby/daxappui/GeqView;->updateUserGain()V

    .line 201
    iput v5, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    .line 202
    invoke-virtual {p0}, Lcom/dolby/daxappui/GeqView;->postInvalidate()V

    goto :goto_6

    .line 150
    :goto_4
    :pswitch_2
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    array-length v0, v0

    if-ge v3, v0, :cond_a

    .line 151
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mXLeftPosition:[I

    aget v0, v0, v3

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mXRightPosition:[I

    aget v0, v0, v3

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_9

    .line 152
    iput v3, p0, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    .line 153
    goto :goto_5

    .line 150
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 156
    :cond_a
    :goto_5
    iput p1, p0, Lcom/dolby/daxappui/GeqView;->mCurrentYPosition:F

    .line 157
    invoke-virtual {p0}, Lcom/dolby/daxappui/GeqView;->postInvalidate()V

    .line 158
    nop

    .line 206
    :cond_b
    :goto_6
    return v4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onUpdateGeqData()V
    .locals 9

    .line 427
    nop

    .line 428
    sget-object v0, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v0, v1}, Lcom/dolby/daxappui/DsClientSettings;->getGraphicEqualizerBandGains(Lcom/dolby/daxappui/IDsFragObserver;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    .line 430
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 434
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 435
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    const/4 v4, 0x2

    mul-int/2addr v4, v0

    aget v3, v3, v4

    int-to-float v3, v3

    const/high16 v5, 0x41800000    # 16.0f

    div-float/2addr v3, v5

    aput v3, v2, v0

    .line 437
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v2, v2, v0

    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v3}, Lcom/dolby/daxappui/DAXConfiguration;->getMaxEditGain()F

    move-result v3

    cmpl-float v2, v2, v3

    const/4 v3, 0x1

    if-lez v2, :cond_1

    .line 438
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v2}, Lcom/dolby/daxappui/DAXConfiguration;->getMaxEditGain()F

    move-result v2

    aput v2, v1, v0

    .line 439
    nop

    .line 444
    :goto_1
    move v1, v3

    goto :goto_2

    .line 440
    :cond_1
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v2, v2, v0

    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v6}, Lcom/dolby/daxappui/DAXConfiguration;->getMinEditGain()F

    move-result v6

    cmpg-float v2, v2, v6

    if-gez v2, :cond_2

    .line 441
    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mConfiguration:Lcom/dolby/daxappui/DAXConfiguration;

    invoke-virtual {v2}, Lcom/dolby/daxappui/DAXConfiguration;->getMinEditGain()F

    move-result v2

    aput v2, v1, v0

    .line 442
    goto :goto_1

    .line 444
    :cond_2
    :goto_2
    if-eqz v1, :cond_5

    .line 445
    const/high16 v2, 0x40000000    # 2.0f

    if-nez v0, :cond_3

    .line 446
    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget-object v4, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v4, v4, v0

    mul-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v3, v0

    .line 447
    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    add-int/lit8 v4, v0, 0x1

    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v6, v6, v0

    iget-object v7, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v7, v7, v4

    add-float/2addr v6, v7

    div-float/2addr v6, v2

    mul-float/2addr v6, v5

    float-to-int v2, v6

    aput v2, v3, v4

    goto :goto_3

    .line 448
    :cond_3
    const/16 v3, 0x9

    if-ne v0, v3, :cond_4

    .line 449
    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    add-int/lit8 v6, v4, -0x1

    iget-object v7, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    add-int/lit8 v8, v0, -0x1

    aget v7, v7, v8

    iget-object v8, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v8, v8, v0

    add-float/2addr v7, v8

    div-float/2addr v7, v2

    mul-float/2addr v7, v5

    float-to-int v2, v7

    aput v2, v3, v6

    .line 450
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v3, v3, v0

    mul-float/2addr v3, v5

    float-to-int v3, v3

    aput v3, v2, v4

    .line 451
    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    add-int/lit8 v4, v4, 0x1

    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v3, v3, v0

    mul-float/2addr v3, v5

    float-to-int v3, v3

    aput v3, v2, v4

    goto :goto_3

    .line 453
    :cond_4
    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    add-int/lit8 v6, v4, -0x1

    iget-object v7, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    add-int/lit8 v8, v0, -0x1

    aget v7, v7, v8

    iget-object v8, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v8, v8, v0

    add-float/2addr v7, v8

    div-float/2addr v7, v2

    mul-float/2addr v7, v5

    float-to-int v7, v7

    aput v7, v3, v6

    .line 454
    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v6, v6, v0

    mul-float/2addr v6, v5

    float-to-int v6, v6

    aput v6, v3, v4

    .line 455
    iget-object v3, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    add-int/lit8 v4, v4, 0x1

    iget-object v6, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    aget v6, v6, v0

    iget-object v7, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    add-int/lit8 v8, v0, 0x1

    aget v7, v7, v8

    add-float/2addr v6, v7

    div-float/2addr v6, v2

    mul-float/2addr v6, v5

    float-to-int v2, v6

    aput v2, v3, v4

    .line 434
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 460
    :cond_6
    if-eqz v1, :cond_7

    .line 461
    sget-object v0, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v1, p0, Lcom/dolby/daxappui/GeqView;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    iget-object v2, p0, Lcom/dolby/daxappui/GeqView;->mUserGain:[I

    invoke-virtual {v0, v1, v2}, Lcom/dolby/daxappui/DsClientSettings;->setGraphicEqualizerBandGains(Lcom/dolby/daxappui/IDsFragObserver;[I)V

    .line 464
    :cond_7
    iget-object v0, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    invoke-virtual {p0, v0}, Lcom/dolby/daxappui/GeqView;->updateThisData([F)V

    .line 465
    return-void

    .line 431
    :cond_8
    :goto_4
    return-void
.end method

.method public updateThisData([F)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/dolby/daxappui/GeqView;->mGeqData:[F

    .line 134
    invoke-virtual {p0}, Lcom/dolby/daxappui/GeqView;->postInvalidate()V

    .line 135
    return-void
.end method
