.class public Lcom/dolby/daxappui/FragMainContent;
.super Landroid/support/v4/app/Fragment;
.source "FragMainContent.java"


# instance fields
.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 27
    iget-object p3, p0, Lcom/dolby/daxappui/FragMainContent;->mView:Landroid/view/View;

    if-eqz p3, :cond_0

    .line 28
    invoke-virtual {p3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    check-cast p3, Landroid/view/ViewGroup;

    if-eqz p3, :cond_0

    .line 30
    iget-object v0, p0, Lcom/dolby/daxappui/FragMainContent;->mView:Landroid/view/View;

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    const p3, 0x7f0c001e    # @layout/content_main 'res/layout/content_main.xml'

    const/4 v0, 0x0

    .line 32
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/dolby/daxappui/FragMainContent;->mView:Landroid/view/View;

    .line 34
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    .line 35
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p2

    .line 36
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p3

    if-eqz p3, :cond_1

    const p3, 0x7f0900a7    # @id/fragProfilePanelTablet

    .line 38
    invoke-virtual {p1, p3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    .line 39
    new-instance v0, Lcom/dolby/daxappui/FragProfilePanel;

    invoke-direct {v0}, Lcom/dolby/daxappui/FragProfilePanel;-><init>()V

    invoke-virtual {p2, p3, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :cond_1
    const p3, 0x7f0900a6    # @id/fragProfilePanel

    .line 42
    invoke-virtual {p1, p3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object p1

    if-nez p1, :cond_2

    .line 43
    new-instance p1, Lcom/dolby/daxappui/FragProfilePresets;

    invoke-direct {p1}, Lcom/dolby/daxappui/FragProfilePresets;-><init>()V

    invoke-virtual {p2, p3, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 45
    :cond_2
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 47
    iget-object p0, p0, Lcom/dolby/daxappui/FragMainContent;->mView:Landroid/view/View;

    return-object p0
.end method

.method public onDestroyView()V
    .locals 1

    .line 52
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 53
    iget-object v0, p0, Lcom/dolby/daxappui/FragMainContent;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 56
    iget-object p0, p0, Lcom/dolby/daxappui/FragMainContent;->mView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method
