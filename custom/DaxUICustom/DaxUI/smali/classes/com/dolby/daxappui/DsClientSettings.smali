.class public Lcom/dolby/daxappui/DsClientSettings;
.super Ljava/lang/Object;
.source "DsClientSettings.java"


# static fields
.field public static final INSTANCE:Lcom/dolby/daxappui/DsClientSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/dolby/daxappui/DsClientSettings;

    invoke-direct {v0}, Lcom/dolby/daxappui/DsClientSettings;-><init>()V

    sput-object v0, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method getDialogEnhancerAmount(Lcom/dolby/daxappui/IDsFragObserver;)I
    .locals 0

    .line 55
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 59
    :cond_0
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getDialogEnhancerAmount()I

    move-result p0

    return p0
.end method

.method getDialogEnhancerOn(Lcom/dolby/daxappui/IDsFragObserver;)Z
    .locals 0

    .line 36
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getDialogEnhancerEnabled()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method getGraphicEqualizerBandGains(Lcom/dolby/daxappui/IDsFragObserver;)[I
    .locals 0

    .line 82
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getGeqBandGains()[I

    move-result-object p0

    return-object p0
.end method

.method getGraphicEqualizerOn(Lcom/dolby/daxappui/IDsFragObserver;I)Z
    .locals 0

    .line 64
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 65
    invoke-virtual {p0, p2}, Lcom/dolby/dax/DolbyAudioEffect;->isGeqEnabled(I)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method getIeqPreset(Lcom/dolby/daxappui/IDsFragObserver;)I
    .locals 1

    .line 111
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    const/4 p1, 0x3

    if-nez p0, :cond_0

    return p1

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getIeqPreset()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    const/4 v0, -0x1

    if-ne p0, v0, :cond_1

    move p0, p1

    :cond_1
    return p0
.end method

.method isProfileSpecificSettingsModified(Lcom/dolby/daxappui/IDsFragObserver;I)Z
    .locals 0

    .line 125
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 126
    invoke-virtual {p0, p2}, Lcom/dolby/dax/DolbyAudioEffect;->isProfileSpecificSettingsModified(I)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public resetAllProfileSpecificSettings(Lcom/dolby/daxappui/IDsFragObserver;)V
    .locals 2

    .line 144
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-nez p0, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const/4 v0, 0x4

    if-ge p1, v0, :cond_2

    .line 149
    invoke-virtual {p0}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    invoke-virtual {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->resetProfileSpecificSettings(I)V

    goto :goto_1

    :cond_1
    const-string v0, "DsClientSettings"

    const-string v1, "Dolby audio effect is out of control in resetAllProfileSpecificSettings"

    .line 152
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method resetProfileSpecificSettings(Lcom/dolby/daxappui/IDsFragObserver;I)V
    .locals 0

    .line 131
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-nez p0, :cond_0

    return-void

    .line 135
    :cond_0
    invoke-virtual {p0}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 136
    invoke-virtual {p0, p2}, Lcom/dolby/dax/DolbyAudioEffect;->resetProfileSpecificSettings(I)V

    goto :goto_0

    :cond_1
    const-string p0, "DsClientSettings"

    const-string p1, "Dolby audio effect is out of control in resetProfileSpecificSettings"

    .line 138
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method setDialogEnhancerAmount(Lcom/dolby/daxappui/IDsFragObserver;I)V
    .locals 0

    .line 42
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-nez p0, :cond_0

    return-void

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getDialogEnhancerAmount()I

    move-result p1

    if-ne p1, p2, :cond_1

    return-void

    .line 47
    :cond_1
    invoke-virtual {p0}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 48
    invoke-virtual {p0, p2}, Lcom/dolby/dax/DolbyAudioEffect;->setDialogEnhancerAmount(I)V

    goto :goto_0

    :cond_2
    const-string p0, "DsClientSettings"

    const-string p1, "Dolby audio effect is out of control in setDialogEnhancerAmount"

    .line 50
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method setDialogEnhancerOn(Lcom/dolby/daxappui/IDsFragObserver;Z)V
    .locals 0

    .line 24
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-nez p0, :cond_0

    return-void

    .line 28
    :cond_0
    invoke-virtual {p0}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 29
    invoke-virtual {p0, p2}, Lcom/dolby/dax/DolbyAudioEffect;->setDialogEnhancerEnabled(Z)V

    goto :goto_0

    :cond_1
    const-string p0, "DsClientSettings"

    const-string p1, "dolbyAudio is out of control in setDialogEnhancerOn()"

    .line 31
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method setGraphicEqualizerBandGains(Lcom/dolby/daxappui/IDsFragObserver;[I)V
    .locals 0

    .line 70
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-nez p0, :cond_0

    return-void

    .line 74
    :cond_0
    invoke-virtual {p0}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 75
    invoke-virtual {p0, p2}, Lcom/dolby/dax/DolbyAudioEffect;->setGeqBandGains([I)V

    goto :goto_0

    :cond_1
    const-string p0, "DsClientSettings"

    const-string p1, "Dolby audio effect is out of control in setGraphicEqualizerBandGains"

    .line 77
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method setIeqPreset(Lcom/dolby/daxappui/IDsFragObserver;I)V
    .locals 0

    .line 91
    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    if-nez p0, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x3

    if-ne p2, p1, :cond_1

    const/4 p2, -0x1

    :cond_1
    add-int/lit8 p2, p2, 0x1

    .line 100
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getIeqPreset()I

    move-result p1

    if-eq p1, p2, :cond_3

    .line 102
    invoke-virtual {p0}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 103
    invoke-virtual {p0, p2}, Lcom/dolby/dax/DolbyAudioEffect;->setIeqPreset(I)V

    goto :goto_0

    :cond_2
    const-string p0, "DsClientSettings"

    const-string p1, "Dolby audio effect is out of control in setIeqPreset"

    .line 105
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_0
    return-void
.end method
