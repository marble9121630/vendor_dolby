.class public Lcom/dolby/daxappui/MainActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "MainActivity.java"

# interfaces
.implements Landroid/support/design/widget/NavigationView$OnNavigationItemSelectedListener;
.implements Lcom/dolby/daxappui/IDsFragObserver;


# static fields
.field private static mAudioManager:Landroid/media/AudioManager;

.field private static methodGetDevicesForStream:Ljava/lang/reflect/Method;


# instance fields
.field private mDevice:I

.field private final mDeviceReceiver:Landroid/content/BroadcastReceiver;

.field private mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

.field private mDolbyIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

.field private mHandler:Landroid/os/Handler;

.field private mHdmiLayout:Landroid/widget/RelativeLayout;

.field private mPowerOffLayout:Landroid/widget/RelativeLayout;

.field private mProductVersion:Ljava/lang/String;

.field private mTabletLayout:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 75
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x0

    .line 79
    iput-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    .line 130
    new-instance v0, Lcom/dolby/daxappui/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/dolby/daxappui/MainActivity$1;-><init>(Lcom/dolby/daxappui/MainActivity;)V

    iput-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 191
    new-instance v0, Lcom/dolby/daxappui/MainActivity$2;

    invoke-direct {v0, p0}, Lcom/dolby/daxappui/MainActivity$2;-><init>(Lcom/dolby/daxappui/MainActivity;)V

    iput-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDeviceReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/dolby/daxappui/MainActivity;)Lcom/dolby/dax/DolbyAudioEffect;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    return-object p0
.end method

.method static synthetic access$002(Lcom/dolby/daxappui/MainActivity;Lcom/dolby/dax/DolbyAudioEffect;)Lcom/dolby/dax/DolbyAudioEffect;
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    return-object p1
.end method

.method static synthetic access$100(Lcom/dolby/daxappui/MainActivity;I)V
    .locals 0

    .line 75
    invoke-direct {p0, p1}, Lcom/dolby/daxappui/MainActivity;->profileSettingsChanged(I)V

    return-void
.end method

.method static synthetic access$202(Lcom/dolby/daxappui/MainActivity;I)I
    .locals 0

    .line 75
    iput p1, p0, Lcom/dolby/daxappui/MainActivity;->mDevice:I

    return p1
.end method

.method static synthetic access$300(Lcom/dolby/daxappui/MainActivity;)I
    .locals 0

    .line 75
    invoke-direct {p0}, Lcom/dolby/daxappui/MainActivity;->getActiveDevices()I

    move-result p0

    return p0
.end method

.method static synthetic access$400(Lcom/dolby/daxappui/MainActivity;)Landroid/support/v4/widget/DrawerLayout;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    return-object p0
.end method

.method static synthetic access$500(Lcom/dolby/daxappui/MainActivity;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity;->mHdmiLayout:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic access$600(Lcom/dolby/daxappui/MainActivity;)Ljava/lang/String;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity;->mProductVersion:Ljava/lang/String;

    return-object p0
.end method

.method private getActiveDevices()I
    .locals 4

    const/4 p0, 0x0

    .line 184
    :try_start_0
    sget-object v0, Lcom/dolby/daxappui/MainActivity;->methodGetDevicesForStream:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/dolby/daxappui/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception v0

    .line 186
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return p0
.end method

.method private profileSettingsChanged(I)V
    .locals 3

    .line 110
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 112
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f090053    # @id/containerView

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dolby/daxappui/FragMainContent;

    if-eqz v0, :cond_1

    .line 115
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0900a6    # @id/fragProfilePanel

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/dolby/daxappui/FragProfilePresets;

    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {v1, p1}, Lcom/dolby/daxappui/FragProfilePresets;->updateProfileSettings(I)V

    .line 120
    :cond_0
    iget-boolean p0, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz p0, :cond_1

    .line 121
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p0

    const v0, 0x7f0900a7    # @id/fragProfilePanelTablet

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/dolby/daxappui/FragProfilePanel;

    if-eqz p0, :cond_1

    .line 123
    invoke-virtual {p0, p1}, Lcom/dolby/daxappui/FragProfilePanel;->updateProfilePanel(I)V

    :cond_1
    return-void
.end method

.method private setInitUIState()V
    .locals 3

    .line 686
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 687
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_1

    .line 689
    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->getDsOn()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dolby/daxappui/MainActivity;->dsPowerChanged(Z)V

    .line 691
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->getNumOfProfiles()I

    move-result v0

    .line 692
    iget-object v1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->getProfile()I

    move-result v1

    const/4 v2, 0x4

    if-gt v2, v1, :cond_0

    if-ge v1, v0, :cond_0

    const/4 v1, 0x0

    .line 696
    :cond_0
    invoke-virtual {p0, v1}, Lcom/dolby/daxappui/MainActivity;->chooseProfile(I)V

    .line 697
    invoke-direct {p0, v1}, Lcom/dolby/daxappui/MainActivity;->profileSettingsChanged(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .line 316
    invoke-static {p1}, Lcom/dolby/daxappui/DAXApplication$ConfigurationWrapper;->wrapLocale(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/app/Activity;->attachBaseContext(Landroid/content/Context;)V

    return-void
.end method

.method public chooseProfile(I)V
    .locals 1

    .line 851
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->getProfile()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 853
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->setProfile(I)V

    goto :goto_0

    :cond_0
    const-string p0, "MainActivity"

    const-string p1, "Dolby audio effect is out of control in chooseProfile"

    .line 856
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method public dsPowerChanged(Z)V
    .locals 12

    const v0, 0x7f090152    # @id/profiletable

    .line 703
    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    const v1, 0x7f090151    # @id/profileViewpager

    .line 704
    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    const v2, 0x7f090148    # @id/presetsListView

    .line 705
    invoke-virtual {p0, v2}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    const v3, 0x7f090143    # @id/powerButtonOn

    .line 706
    invoke-virtual {p0, v3}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    const v4, 0x7f09019d    # @id/titleBar

    const v5, 0x7f0900a7    # @id/fragProfilePanelTablet

    const v6, 0x7f090053    # @id/containerView

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz p1, :cond_9

    .line 710
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_0

    .line 711
    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    .line 713
    iget-object v9, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v9}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 717
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v9, 0x7f070063    # @drawable/btn_power_on_titlebar 'res/drawable/btn_power_on_titlebar.xml'

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v10

    invoke-virtual {p1, v9, v10}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 718
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 721
    iget-boolean v3, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz v3, :cond_1

    .line 722
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const-string v9, "#161819"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-direct {v3, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v3}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 724
    :cond_1
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const-string v9, "#1E2426"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-direct {v3, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v3}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 726
    :goto_0
    invoke-virtual {p1, v7}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 727
    invoke-virtual {p1, v7}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 728
    invoke-virtual {p1, v7}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    :cond_2
    const p1, 0x7f050059    # @color/colorTitleBar '@android:color/system_neutral1_50'

    if-eqz v0, :cond_4

    .line 733
    invoke-virtual {v0, v8}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 734
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v10

    invoke-virtual {v9, p1, v10}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v9

    invoke-virtual {v0, v9}, Landroid/widget/HorizontalScrollView;->setBackgroundColor(I)V

    .line 735
    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    move v9, v8

    .line 736
    :goto_1
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    if-ge v9, v10, :cond_3

    .line 737
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10, v7}, Landroid/view/View;->setClickable(Z)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 740
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v9, 0x7f050048    # @color/colorProfileDesText '#94000000'

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050051    # @color/colorSelectedTabText '@android:color/primary_text_light'

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v9

    invoke-virtual {v0, v3, v9}, Landroid/support/design/widget/TabLayout;->setTabTextColors(II)V

    const/high16 v3, 0x40800000    # 4.0f

    .line 741
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v9, v3

    float-to-int v3, v9

    invoke-virtual {v0, v3}, Landroid/support/design/widget/TabLayout;->setSelectedTabIndicatorHeight(I)V

    :cond_4
    if-eqz v1, :cond_5

    .line 746
    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_5
    if-eqz v2, :cond_6

    .line 751
    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 754
    :cond_6
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 756
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dolby/daxappui/FragMainContent;

    if-eqz v0, :cond_8

    .line 757
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 758
    iget-boolean v1, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz v1, :cond_8

    .line 759
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/dolby/daxappui/FragProfilePanel;

    if-eqz v1, :cond_7

    .line 760
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 761
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 764
    :cond_7
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 765
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 771
    :cond_8
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p1, v8}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 773
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {p1, v7}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 774
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarDrawerToggle;->syncState()V

    goto/16 :goto_4

    .line 777
    :cond_9
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p1, v7}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 780
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result p1

    const v9, 0x7f05002d    # @color/dup_0x7f05002d 'false'

    const/4 v10, 0x4

    if-nez p1, :cond_b

    .line 782
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/dolby/daxappui/FragMainContent;

    if-eqz p1, :cond_b

    .line 783
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 784
    iget-boolean v6, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz v6, :cond_b

    .line 785
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v5

    check-cast v5, Lcom/dolby/daxappui/FragProfilePanel;

    if-eqz v5, :cond_a

    .line 786
    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 787
    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 790
    :cond_a
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    .line 791
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v4}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_b
    if-eqz v2, :cond_c

    .line 798
    invoke-virtual {v2, v8}, Landroid/widget/ListView;->setEnabled(Z)V

    :cond_c
    if-eqz v1, :cond_d

    .line 803
    invoke-virtual {v1, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_d
    const p1, 0x7f050040    # @color/colorNavBackground '@android:color/system_neutral1_50'

    if-eqz v0, :cond_f

    .line 808
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setBackgroundColor(I)V

    .line 809
    invoke-virtual {v0, v8}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 810
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    move v2, v8

    .line 811
    :goto_2
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_e

    .line 812
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/View;->setClickable(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 815
    :cond_e
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v4, 0x7f05004b    # @color/colorProfileTextOff '#2f000000'

    invoke-virtual {v1, v4, v2}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/TabLayout;->setTabTextColors(II)V

    .line 816
    invoke-virtual {v0, v8}, Landroid/support/design/widget/TabLayout;->setSelectedTabIndicatorHeight(I)V

    .line 819
    :cond_f
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 821
    invoke-virtual {v0, v8}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 822
    invoke-virtual {v0, v7}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 823
    invoke-virtual {v0, v8}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 824
    iget-object v1, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->syncState()V

    .line 825
    iget-boolean v1, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz v1, :cond_10

    .line 826
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v1, v9, v2}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-direct {p1, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 828
    :cond_10
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v2, p1, v4}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p1

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 833
    :cond_11
    :goto_3
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz p1, :cond_13

    .line 834
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_12

    .line 836
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 838
    :cond_12
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 840
    :cond_13
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f070062    # @drawable/btn_power_off_titlebar 'res/drawable/btn_power_off_titlebar.xml'

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    invoke-virtual {p1, v0, p0}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_4
    return-void
.end method

.method public getActivePort()I
    .locals 1

    .line 863
    iget p0, p0, Lcom/dolby/daxappui/MainActivity;->mDevice:I

    const/4 v0, 0x2

    if-eq p0, v0, :cond_5

    const/4 v0, 0x4

    if-eq p0, v0, :cond_4

    const/16 v0, 0x8

    if-eq p0, v0, :cond_4

    const/16 v0, 0x80

    if-eq p0, v0, :cond_3

    const/16 v0, 0x400

    if-eq p0, v0, :cond_2

    const/16 v0, 0x4000

    if-eq p0, v0, :cond_1

    const v0, 0x8000

    if-eq p0, v0, :cond_0

    .line 884
    sget p0, Lcom/dolby/daxappui/Constants;->DEFAULT_SPEAKER_PORT:I

    goto :goto_0

    .line 881
    :cond_0
    sget p0, Lcom/dolby/daxappui/Constants;->DEFAULT_MIRACAST_PORT:I

    goto :goto_0

    .line 865
    :cond_1
    sget p0, Lcom/dolby/daxappui/Constants;->DEFAULT_USB_PORT:I

    goto :goto_0

    .line 878
    :cond_2
    sget p0, Lcom/dolby/daxappui/Constants;->DEFAULT_HDMI_PORT:I

    goto :goto_0

    .line 868
    :cond_3
    sget p0, Lcom/dolby/daxappui/Constants;->DEFAULT_BLUETOOTH_PORT:I

    goto :goto_0

    .line 872
    :cond_4
    sget p0, Lcom/dolby/daxappui/Constants;->DEFAULT_HEADPHONE_PORT:I

    goto :goto_0

    .line 875
    :cond_5
    sget p0, Lcom/dolby/daxappui/Constants;->DEFAULT_SPEAKER_PORT:I

    :goto_0
    return p0
.end method

.method public getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;
    .locals 0

    .line 846
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    return-object p0
.end method

.method public synthetic lambda$onCreate$0$MainActivity(Landroid/view/View;)V
    .locals 0

    .line 411
    invoke-virtual {p0}, Lcom/dolby/daxappui/MainActivity;->onBackPressed()V

    return-void
.end method

.method public onBackPressed()V
    .locals 5

    .line 562
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    return-void

    .line 567
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 568
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 569
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_1

    .line 571
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 572
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    const v1, 0x7f090080    # @id/dolbyToolbar

    .line 574
    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    .line 575
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const v1, 0x7f090085    # @id/dsLogoText

    .line 577
    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 579
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 582
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 585
    iget-boolean v3, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz v3, :cond_3

    .line 586
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const-string v4, "#161819"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 588
    :cond_3
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const-string v4, "#1E2426"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 591
    :goto_0
    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 593
    :cond_4
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 594
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_5

    .line 596
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 598
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 599
    invoke-direct {p0}, Lcom/dolby/daxappui/MainActivity;->setInitUIState()V

    :cond_5
    :goto_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .line 321
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 336
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dolby/daxappui/DAXApplication;->getProductVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mProductVersion:Ljava/lang/String;

    .line 337
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mProductVersion:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string p0, "MainActivity"

    const-string p1, "Could not get the product version"

    .line 338
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 341
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 342
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "com.dolby.intent.action.DAP_PARAMS_UPDATE"

    .line 343
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "audio_server_restarted"

    .line 344
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 345
    iget-object v3, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 348
    new-instance v1, Lcom/dolby/dax/DolbyAudioEffect;

    invoke-direct {v1, v2, v2}, Lcom/dolby/dax/DolbyAudioEffect;-><init>(II)V

    iput-object v1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 351
    :try_start_0
    const-class v4, Landroid/media/AudioManager;

    invoke-virtual {v4}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v4

    .line 353
    array-length v5, v4

    move-object v7, v1

    move v6, v2

    :goto_0
    if-ge v6, v5, :cond_2

    aget-object v7, v4, v6

    .line 355
    invoke-virtual {v7}, Ljava/lang/reflect/Constructor;->getGenericParameterTypes()[Ljava/lang/reflect/Type;

    move-result-object v8

    array-length v8, v8

    if-nez v8, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 359
    :cond_2
    :goto_1
    invoke-virtual {v7, v3}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    new-array v4, v2, [Ljava/lang/Object;

    .line 360
    invoke-virtual {v7, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    sput-object v4, Lcom/dolby/daxappui/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 361
    sget-object v4, Lcom/dolby/daxappui/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "getDevicesForStream"

    new-array v6, v3, [Ljava/lang/Class;

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    sput-object v4, Lcom/dolby/daxappui/MainActivity;->methodGetDevicesForStream:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v4

    .line 363
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 366
    :goto_2
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    const-string v5, "android.intent.action.HEADSET_PLUG"

    .line 367
    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v5, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    .line 368
    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v5, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    .line 369
    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v5, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    .line 370
    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v5, "android.bluetooth.adapter.action.STATE_CHANGED"

    .line 371
    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/16 v5, 0x1c

    .line 372
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v5, v6, :cond_3

    const-string v5, "android.media.action.HDMI_AUDIO_PLUG"

    .line 373
    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    :try_start_1
    const-string v5, "android.view.WindowManagerPolicy"

    .line 376
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const-string v6, "ACTION_HDMI_PLUGGED"

    .line 377
    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 378
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    .line 379
    const-class v7, Ljava/lang/String;

    if-ne v6, v7, :cond_4

    .line 380
    invoke-virtual {v5, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v5

    .line 385
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 388
    :cond_4
    :goto_3
    iget-object v5, p0, Lcom/dolby/daxappui/MainActivity;->mDeviceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v4}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 392
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    .line 393
    iget-boolean v4, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz v4, :cond_5

    .line 394
    invoke-virtual {p0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_4

    .line 396
    :cond_5
    invoke-virtual {p0, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :goto_4
    const v4, 0x7f0c001c    # @layout/activity_main 'res/layout/activity_main.xml'

    .line 398
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatActivity;->setContentView(I)V

    const v4, 0x7f0901a4    # @id/toolbar

    .line 399
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Landroid/support/v7/widget/Toolbar;

    .line 400
    invoke-virtual {p0, v8}, Landroid/support/v7/app/AppCompatActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 401
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 402
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 403
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 404
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_6
    const v4, 0x7f090083    # @id/drawer_layout

    .line 407
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v4/widget/DrawerLayout;

    iput-object v4, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 408
    new-instance v4, Landroid/support/v7/app/ActionBarDrawerToggle;

    iget-object v7, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v9, 0x7f10007e    # @string/navigation_drawer_open 'Open navigation drawer'

    const v10, 0x7f10007d    # @string/navigation_drawer_close 'Close navigation drawer'

    move-object v5, v4

    move-object v6, p0

    invoke-direct/range {v5 .. v10}, Landroid/support/v7/app/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V

    iput-object v4, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    .line 411
    iget-object v4, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    new-instance v5, Lcom/dolby/daxappui/-$$Lambda$MainActivity$HSGO6IFSkcelc_691wXyeHK-KLk;

    invoke-direct {v5, p0}, Lcom/dolby/daxappui/-$$Lambda$MainActivity$HSGO6IFSkcelc_691wXyeHK-KLk;-><init>(Lcom/dolby/daxappui/MainActivity;)V

    invoke-virtual {v4, v5}, Landroid/support/v7/app/ActionBarDrawerToggle;->setToolbarNavigationClickListener(Landroid/view/View$OnClickListener;)V

    .line 413
    iget-object v4, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->addDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 414
    iget-object v4, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v4}, Landroid/support/v7/app/ActionBarDrawerToggle;->syncState()V

    const v4, 0x7f09012a    # @id/nav_view

    .line 416
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/design/widget/NavigationView;

    .line 417
    invoke-virtual {v4, p0}, Landroid/support/design/widget/NavigationView;->setNavigationItemSelectedListener(Landroid/support/design/widget/NavigationView$OnNavigationItemSelectedListener;)V

    .line 418
    invoke-virtual {v4, v1}, Landroid/support/design/widget/NavigationView;->setItemIconTintList(Landroid/content/res/ColorStateList;)V

    .line 420
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    const v6, 0x7f090053    # @id/containerView

    .line 421
    new-instance v7, Lcom/dolby/daxappui/FragMainContent;

    invoke-direct {v7}, Lcom/dolby/daxappui/FragMainContent;-><init>()V

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 424
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f0c0081    # @layout/power_off 'res/layout/power_off.xml'

    invoke-virtual {v5, v6, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f090147    # @id/poweroffLayout

    .line 425
    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    .line 426
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    invoke-direct {v5, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 427
    iget-object v7, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 429
    iget-object v5, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v5, :cond_8

    .line 430
    iget-object v5, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    if-eqz v5, :cond_7

    .line 432
    iget-object v7, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v7}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 434
    :cond_7
    iget-object v5, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v7, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 437
    :cond_8
    iget-boolean v5, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz v5, :cond_9

    .line 438
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 440
    new-instance v7, Landroid/graphics/drawable/ColorDrawable;

    const-string v8, "#161819"

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-direct {v7, v8}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v5, v7}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_9
    const v5, 0x7f090146    # @id/powerOffText

    .line 444
    invoke-virtual {p0, v5}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const-string v7, "DS1"

    .line 445
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 446
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v7, 0x7f10003a    # @string/ds1_power_off_text 'Dolby Audio is turned off.'

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 448
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v7, 0x7f100086    # @string/power_off_text 'Dolby Atmos is turned off.'

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450
    :goto_5
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mHandler:Landroid/os/Handler;

    if-eqz p1, :cond_c

    const-string v0, "currentNavItem"

    .line 453
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v3, :cond_c

    .line 455
    iget-object v3, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_b

    .line 456
    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_b

    .line 458
    iget-object v5, p0, Lcom/dolby/daxappui/MainActivity;->mPowerOffLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 461
    :cond_b
    invoke-virtual {v4}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-interface {v3, p1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/dolby/daxappui/MainActivity;->onNavigationItemSelected(Landroid/view/MenuItem;)Z

    :cond_c
    const p1, 0x7f09010e    # @id/mainLayout

    .line 464
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 465
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v3, Lcom/dolby/daxappui/MainActivity$3;

    invoke-direct {v3, p0, p1, v4}, Lcom/dolby/daxappui/MainActivity$3;-><init>(Lcom/dolby/daxappui/MainActivity;Landroid/widget/LinearLayout;Landroid/support/design/widget/NavigationView;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 481
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0c0032    # @layout/hdmi_page 'res/layout/hdmi_page.xml'

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 516
    const v0, 0x7f0900cc    # @id/hdmi_layout

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mHdmiLayout:Landroid/widget/RelativeLayout;

    .line 517
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 518
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mHdmiLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 520
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mHdmiLayout:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_d

    .line 521
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mHdmiLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 522
    if-eqz p1, :cond_d

    .line 523
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mHdmiLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 528
    :cond_d
    invoke-virtual {v4}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object p1

    return-void

    :catch_2
    move-exception p0

    .line 383
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method protected onDestroy()V
    .locals 1

    .line 551
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 552
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_0

    .line 553
    invoke-virtual {v0}, Landroid/media/audiofx/AudioEffect;->release()V

    const/4 v0, 0x0

    .line 554
    iput-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 557
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDeviceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onNavigationItemSelected(Landroid/view/MenuItem;)Z
    .locals 8

    .line 631
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f090128    # @id/nav_reset

    const v2, 0x7f090126    # @id/nav_exploredolby

    const/4 v3, 0x0

    const v4, 0x7f090053    # @id/containerView

    if-ne v0, v2, :cond_0

    .line 635
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v6

    .line 636
    new-instance v7, Lcom/dolby/daxappui/exploreDolby/FragExploreDolbyAtmos;

    invoke-direct {v7}, Lcom/dolby/daxappui/exploreDolby/FragExploreDolbyAtmos;-><init>()V

    invoke-virtual {v6, v4, v7}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v6, v3}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    :cond_0
    const v6, 0x7f090129    # @id/nav_tutorial

    if-ne v0, v6, :cond_1

    .line 639
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    if-ne v0, v1, :cond_2

    .line 642
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v6

    .line 643
    new-instance v7, Lcom/dolby/daxappui/reset/FragReset;

    invoke-direct {v7}, Lcom/dolby/daxappui/reset/FragReset;-><init>()V

    invoke-virtual {v6, v4, v7}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v6, v3}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    :cond_2
    :goto_0
    const/4 v4, 0x1

    if-eq v0, v2, :cond_3

    if-ne v0, v1, :cond_6

    :cond_3
    const v0, 0x7f090080    # @id/dolbyToolbar

    .line 652
    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    .line 653
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const v0, 0x7f090085    # @id/dsLogoText

    .line 655
    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 657
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 660
    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 663
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f05002c    # @color/colorActionBar '@android:color/system_neutral1_200'

    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 665
    invoke-virtual {v0, v4}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 666
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-direct {v2, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 667
    new-instance p1, Landroid/text/style/TypefaceSpan;

    const-string v3, "fonts/SourceSansPro-Regular.otf"

    invoke-direct {p1, v3}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v5, 0x21

    invoke-virtual {v2, p1, v1, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 668
    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 671
    :cond_5
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {p1, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 673
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p1, v4}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 676
    :cond_6
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/dolby/daxappui/MainActivity$4;

    invoke-direct {v0, p0}, Lcom/dolby/daxappui/MainActivity$4;-><init>(Lcom/dolby/daxappui/MainActivity;)V

    const-wide/16 v1, 0xfa

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return v4
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 0

    .line 624
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p0

    return p0
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "MainActivity"

    const-string v1, "onPause()"

    .line 500
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 502
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_0

    .line 503
    invoke-virtual {v0}, Landroid/media/audiofx/AudioEffect;->release()V

    const/4 v0, 0x0

    .line 504
    iput-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    const-string v0, "MainActivity"

    const-string v1, "onResume()"

    .line 510
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 512
    iget-object v1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-nez v1, :cond_0

    .line 513
    new-instance v1, Lcom/dolby/dax/DolbyAudioEffect;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2}, Lcom/dolby/dax/DolbyAudioEffect;-><init>(II)V

    iput-object v1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    .line 514
    iget-object v1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v1}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Dolby audio effect is out of control"

    .line 515
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 520
    invoke-direct {p0}, Lcom/dolby/daxappui/MainActivity;->setInitUIState()V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 606
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const v0, 0x7f09012a    # @id/nav_view

    .line 607
    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/support/design/widget/NavigationView;

    .line 608
    invoke-virtual {p0}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object p0

    const/4 v0, 0x0

    .line 609
    :goto_0
    invoke-interface {p0}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 610
    invoke-interface {p0, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 611
    invoke-interface {v1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "currentNavItem"

    .line 612
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 1

    .line 526
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStart()V

    .line 527
    invoke-direct {p0}, Lcom/dolby/daxappui/MainActivity;->getActiveDevices()I

    move-result v0

    iput v0, p0, Lcom/dolby/daxappui/MainActivity;->mDevice:I

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .line 532
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onWindowFocusChanged("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MainActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_1

    .line 534
    iget-object v0, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-nez v0, :cond_1

    .line 535
    new-instance p1, Lcom/dolby/dax/DolbyAudioEffect;

    const/4 v0, 0x0

    invoke-direct {p1, v0, v0}, Lcom/dolby/dax/DolbyAudioEffect;-><init>(II)V

    iput-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    .line 536
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {p1}, Landroid/media/audiofx/AudioEffect;->hasControl()Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "Dolby audio effect is out of control"

    .line 537
    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result p1

    if-nez p1, :cond_2

    .line 541
    invoke-direct {p0}, Lcom/dolby/daxappui/MainActivity;->setInitUIState()V

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    .line 543
    iget-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz p1, :cond_2

    .line 544
    invoke-virtual {p1}, Landroid/media/audiofx/AudioEffect;->release()V

    const/4 p1, 0x0

    .line 545
    iput-object p1, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    :cond_2
    :goto_0
    return-void
.end method

.method public resetProfile(I)V
    .locals 0

    .line 892
    invoke-direct {p0, p1}, Lcom/dolby/daxappui/MainActivity;->profileSettingsChanged(I)V

    return-void
.end method

.method setGeqViewEnabled()V
    .locals 3

    .line 294
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 296
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f090053    # @id/containerView

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dolby/daxappui/FragMainContent;

    if-eqz v0, :cond_1

    .line 299
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0900a6    # @id/fragProfilePanel

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/dolby/daxappui/FragProfilePresets;

    if-eqz v1, :cond_0

    .line 300
    iget-object v2, p0, Lcom/dolby/daxappui/MainActivity;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v2, :cond_0

    .line 301
    invoke-virtual {v2}, Lcom/dolby/dax/DolbyAudioEffect;->getProfile()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/dolby/daxappui/FragProfilePresets;->setGeqViewEnabled(I)V

    .line 304
    :cond_0
    iget-boolean p0, p0, Lcom/dolby/daxappui/MainActivity;->mTabletLayout:Z

    if-eqz p0, :cond_1

    .line 305
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p0

    const v0, 0x7f0900a7    # @id/fragProfilePanelTablet

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/dolby/daxappui/FragProfilePanel;

    if-eqz p0, :cond_1

    .line 307
    invoke-virtual {p0}, Lcom/dolby/daxappui/FragProfilePanel;->setGeqViewEnabled()V

    :cond_1
    return-void
.end method
