.class Lcom/dolby/daxappui/tutorial/TutorialActivity$1;
.super Ljava/lang/Object;
.source "TutorialActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dolby/daxappui/tutorial/TutorialActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

.field final synthetic val$mProductVersion:Ljava/lang/String;

.field final synthetic val$mainLayout:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/dolby/daxappui/tutorial/TutorialActivity;Landroid/widget/LinearLayout;Ljava/lang/String;)V
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    iput-object p2, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;->val$mainLayout:Landroid/widget/LinearLayout;

    iput-object p3, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;->val$mProductVersion:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .line 337
    iget-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;->val$mainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 338
    iget-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    const v1, 0x7f090085    # @id/dsLogoText

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 339
    iget-object v1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;->val$mProductVersion:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "DS1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 340
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v1, 0x7f100029    # @string/app_name_ds1 'Dolby Audio'

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 342
    :cond_0
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v1, 0x7f100028    # @string/app_name 'Dolby Atmos'

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
