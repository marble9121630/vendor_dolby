.class public final enum Lcom/dolby/dax/DsTuning;
.super Ljava/lang/Enum;
.source "DsTuning.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/dolby/dax/DsTuning;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dolby/dax/DsTuning;

.field public static final PORT_IDS:[I

.field public static final PORT_NAMES:[Ljava/lang/String;

.field public static final enum bluetooth:Lcom/dolby/dax/DsTuning;

.field public static final enum hdmi:Lcom/dolby/dax/DsTuning;

.field public static final enum headphone:Lcom/dolby/dax/DsTuning;

.field public static final enum internal_speaker:Lcom/dolby/dax/DsTuning;

.field public static final enum miracast:Lcom/dolby/dax/DsTuning;

.field public static final enum usb:Lcom/dolby/dax/DsTuning;


# instance fields
.field private id_:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .line 18
    new-instance v0, Lcom/dolby/dax/DsTuning;

    const/4 v1, 0x0

    const-string v2, "internal_speaker"

    invoke-direct {v0, v2, v1, v1}, Lcom/dolby/dax/DsTuning;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    .line 19
    new-instance v0, Lcom/dolby/dax/DsTuning;

    const/4 v2, 0x1

    const-string v3, "hdmi"

    invoke-direct {v0, v3, v2, v2}, Lcom/dolby/dax/DsTuning;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsTuning;->hdmi:Lcom/dolby/dax/DsTuning;

    .line 20
    new-instance v0, Lcom/dolby/dax/DsTuning;

    const/4 v3, 0x2

    const-string v4, "miracast"

    invoke-direct {v0, v4, v3, v3}, Lcom/dolby/dax/DsTuning;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsTuning;->miracast:Lcom/dolby/dax/DsTuning;

    .line 21
    new-instance v0, Lcom/dolby/dax/DsTuning;

    const/4 v4, 0x3

    const-string v5, "headphone"

    invoke-direct {v0, v5, v4, v4}, Lcom/dolby/dax/DsTuning;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsTuning;->headphone:Lcom/dolby/dax/DsTuning;

    .line 22
    new-instance v0, Lcom/dolby/dax/DsTuning;

    const/4 v5, 0x4

    const-string v6, "bluetooth"

    invoke-direct {v0, v6, v5, v5}, Lcom/dolby/dax/DsTuning;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsTuning;->bluetooth:Lcom/dolby/dax/DsTuning;

    .line 23
    new-instance v0, Lcom/dolby/dax/DsTuning;

    const/4 v6, 0x5

    const-string v7, "usb"

    invoke-direct {v0, v7, v6, v6}, Lcom/dolby/dax/DsTuning;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    const/4 v0, 0x6

    new-array v7, v0, [Lcom/dolby/dax/DsTuning;

    .line 16
    sget-object v8, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    aput-object v8, v7, v1

    sget-object v9, Lcom/dolby/dax/DsTuning;->hdmi:Lcom/dolby/dax/DsTuning;

    aput-object v9, v7, v2

    sget-object v9, Lcom/dolby/dax/DsTuning;->miracast:Lcom/dolby/dax/DsTuning;

    aput-object v9, v7, v3

    sget-object v9, Lcom/dolby/dax/DsTuning;->headphone:Lcom/dolby/dax/DsTuning;

    aput-object v9, v7, v4

    sget-object v9, Lcom/dolby/dax/DsTuning;->bluetooth:Lcom/dolby/dax/DsTuning;

    aput-object v9, v7, v5

    sget-object v9, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    aput-object v9, v7, v6

    sput-object v7, Lcom/dolby/dax/DsTuning;->$VALUES:[Lcom/dolby/dax/DsTuning;

    const-string v10, "internal_speaker"

    const-string v11, "hdmi"

    const-string v12, "miracast"

    const-string v13, "headphone"

    const-string v14, "bluetooth"

    const-string v15, "usb"

    const-string v16, "other"

    .line 30
    filled-new-array/range {v10 .. v16}, [Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/dolby/dax/DsTuning;->PORT_NAMES:[Ljava/lang/String;

    new-array v0, v0, [I

    .line 44
    invoke-virtual {v8}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v7

    aput v7, v0, v1

    sget-object v1, Lcom/dolby/dax/DsTuning;->hdmi:Lcom/dolby/dax/DsTuning;

    .line 45
    invoke-virtual {v1}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v1

    aput v1, v0, v2

    sget-object v1, Lcom/dolby/dax/DsTuning;->miracast:Lcom/dolby/dax/DsTuning;

    .line 46
    invoke-virtual {v1}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/dolby/dax/DsTuning;->headphone:Lcom/dolby/dax/DsTuning;

    .line 47
    invoke-virtual {v1}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v1

    aput v1, v0, v4

    sget-object v1, Lcom/dolby/dax/DsTuning;->bluetooth:Lcom/dolby/dax/DsTuning;

    .line 48
    invoke-virtual {v1}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    .line 49
    invoke-virtual {v1}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v1

    aput v1, v0, v6

    sput-object v0, Lcom/dolby/dax/DsTuning;->PORT_IDS:[I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dolby/dax/DsTuning;->id_:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dolby/dax/DsTuning;
    .locals 1

    .line 16
    const-class v0, Lcom/dolby/dax/DsTuning;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/dolby/dax/DsTuning;

    return-object p0
.end method

.method public static values()[Lcom/dolby/dax/DsTuning;
    .locals 1

    .line 16
    sget-object v0, Lcom/dolby/dax/DsTuning;->$VALUES:[Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, [Lcom/dolby/dax/DsTuning;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dolby/dax/DsTuning;

    return-object v0
.end method


# virtual methods
.method public toInt()I
    .locals 0

    .line 26
    iget p0, p0, Lcom/dolby/dax/DsTuning;->id_:I

    return p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 57
    sget-object v0, Lcom/dolby/dax/DsTuning;->PORT_NAMES:[Ljava/lang/String;

    iget p0, p0, Lcom/dolby/dax/DsTuning;->id_:I

    aget-object p0, v0, p0

    return-object p0
.end method
