.class public Lcom/dolby/dax/DolbyAudioEffect;
.super Landroid/media/audiofx/AudioEffect;
.source "DolbyAudioEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dolby/dax/DolbyAudioEffect$BaseParameterListener;,
        Lcom/dolby/dax/DolbyAudioEffect$OnParameterChangeListener;
    }
.end annotation


# static fields
.field public static final EFFECT_TYPE_DOLBY_AUDIO_PROCESSING:Ljava/util/UUID;

.field private static sNumOfProfiles:I


# instance fields
.field private mBaseParamListener:Lcom/dolby/dax/DolbyAudioEffect$BaseParameterListener;

.field private mParamListener:Lcom/dolby/dax/DolbyAudioEffect$OnParameterChangeListener;

.field private final mParamListenerLock:Ljava/lang/Object;

.field private mProfileSupported:Z

.field private mSessionId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "ro.dolby.mod_uuid"

    const-string v1, "false"

    .line 1
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    .line 2
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "9d4921da-8225-4f29-aefa-39537a04bcaa"

    goto :goto_0

    :cond_0
    const-string v0, "9d4921da-8225-4f29-aefa-5f7279756b69"

    .line 49
    :goto_0
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/dolby/dax/DolbyAudioEffect;->EFFECT_TYPE_DOLBY_AUDIO_PROCESSING:Ljava/util/UUID;

    const/4 v0, 0x0

    .line 67
    sput v0, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .line 111
    sget-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_NULL:Ljava/util/UUID;

    sget-object v1, Lcom/dolby/dax/DolbyAudioEffect;->EFFECT_TYPE_DOLBY_AUDIO_PROCESSING:Ljava/util/UUID;

    invoke-direct {p0, v0, v1, p1, p2}, Landroid/media/audiofx/AudioEffect;-><init>(Ljava/util/UUID;Ljava/util/UUID;II)V

    const/4 p1, 0x1

    .line 72
    iput-boolean p1, p0, Lcom/dolby/dax/DolbyAudioEffect;->mProfileSupported:Z

    const/4 p1, 0x0

    .line 82
    iput-object p1, p0, Lcom/dolby/dax/DolbyAudioEffect;->mParamListener:Lcom/dolby/dax/DolbyAudioEffect$OnParameterChangeListener;

    .line 87
    iput-object p1, p0, Lcom/dolby/dax/DolbyAudioEffect;->mBaseParamListener:Lcom/dolby/dax/DolbyAudioEffect$BaseParameterListener;

    .line 92
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/dolby/dax/DolbyAudioEffect;->mParamListenerLock:Ljava/lang/Object;

    if-nez p2, :cond_0

    const-string p1, "DolbyAudioEffect"

    const-string v0, "Creating a DolbyAudioEffect to global output mix!"

    .line 114
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    iput p2, p0, Lcom/dolby/dax/DolbyAudioEffect;->mSessionId:I

    .line 118
    sget p1, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    if-nez p1, :cond_1

    .line 119
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getNumOfProfiles()I

    move-result p0

    sput p0, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    :cond_1
    return-void
.end method

.method private static byteArrayToInt32([B)I
    .locals 2

    const/4 v0, 0x3

    .line 150
    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    const/4 v1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    const/4 v1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    const/4 v1, 0x0

    aget-byte p0, p0, v1

    and-int/lit16 p0, p0, 0xff

    or-int/2addr p0, v0

    return p0
.end method

.method private static byteArrayToInt32Array([BII)[I
    .locals 5

    .line 138
    array-length v0, p0

    sub-int/2addr v0, p1

    shr-int/lit8 v0, v0, 0x2

    if-le v0, p2, :cond_0

    goto :goto_0

    :cond_0
    move p2, v0

    .line 140
    :goto_0
    new-array v0, p2, [I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p2, :cond_1

    mul-int/lit8 v2, v1, 0x4

    add-int/lit8 v3, v2, 0x3

    add-int/2addr v3, p1

    .line 143
    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    add-int/lit8 v4, v2, 0x2

    add-int/2addr v4, p1

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v3, v4

    add-int/lit8 v4, v2, 0x1

    add-int/2addr v4, p1

    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v3, v4

    add-int/2addr v2, p1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v3

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method private checkReturnValue(I)V
    .locals 0

    if-gez p1, :cond_2

    const/4 p0, -0x5

    if-eq p1, p0, :cond_1

    const/4 p0, -0x4

    if-eq p1, p0, :cond_0

    .line 260
    new-instance p0, Ljava/lang/RuntimeException;

    const-string p1, "DolbyAudioEffect: set/get parameter error"

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 254
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "DolbyAudioEffect: bad parameter value"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 257
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "DolbyAudioEffect: invalid parameter operation"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    return-void
.end method

.method private getPortDeviceNameLength(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 819
    sget-object v0, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-lt p1, v0, :cond_0

    sget-object v0, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [B

    shl-int/lit8 p1, p1, 0x10

    add-int/lit8 p1, p1, 0x2

    .line 826
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->getParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    .line 827
    invoke-static {v0}, Lcom/dolby/dax/DolbyAudioEffect;->byteArrayToInt32([B)I

    move-result p0

    return p0

    .line 820
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in getPortDeviceNameLength(): Invalid port"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method private static int32ArrayToByteArray([I[BI)I
    .locals 5

    .line 163
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 166
    aget v2, p0, v1

    add-int/lit8 v3, p2, 0x1

    ushr-int/lit8 v4, v2, 0x0

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    .line 167
    aput-byte v4, p1, p2

    add-int/lit8 p2, v3, 0x1

    ushr-int/lit8 v4, v2, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    .line 168
    aput-byte v4, p1, v3

    add-int/lit8 v3, p2, 0x1

    ushr-int/lit8 v4, v2, 0x10

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    .line 169
    aput-byte v4, p1, p2

    add-int/lit8 p2, v3, 0x1

    ushr-int/lit8 v2, v2, 0x18

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    .line 170
    aput-byte v2, p1, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    shl-int/lit8 p0, v0, 0x2

    return p0
.end method

.method private static int32ToByteArray(I[BI)I
    .locals 2

    add-int/lit8 v0, p2, 0x1

    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    .line 155
    aput-byte v1, p1, p2

    add-int/lit8 p2, v0, 0x1

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    .line 156
    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x1

    ushr-int/lit8 v1, p0, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    .line 157
    aput-byte v1, p1, p2

    ushr-int/lit8 p0, p0, 0x18

    and-int/lit16 p0, p0, 0xff

    int-to-byte p0, p0

    .line 158
    aput-byte p0, p1, v0

    const/4 p0, 0x4

    return p0
.end method


# virtual methods
.method protected getBoolParam(I)Z
    .locals 2

    const/16 v0, 0xc

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 227
    invoke-static {p1, v0, v1}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    add-int/lit8 p1, p1, 0x5

    .line 228
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->getParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    .line 230
    invoke-static {v0}, Lcom/dolby/dax/DolbyAudioEffect;->byteArrayToInt32([B)I

    move-result p0

    if-lez p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public getDapParameter(I)[I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 699
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getProfile()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->getDapParameter(II)[I

    move-result-object p0

    return-object p0
.end method

.method public getDapParameter(II)[I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-ltz p1, :cond_0

    .line 712
    sget v0, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    if-ge p1, v0, :cond_0

    .line 719
    :try_start_0
    sget-object v0, Lcom/dolby/dax/DsParams;->kParamToLen:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x1

    :goto_0
    add-int/lit8 v1, v0, 0x2

    mul-int/lit8 v1, v1, 0x4

    .line 724
    new-array v1, v1, [B

    shl-int/lit8 p2, p2, 0x10

    shl-int/lit8 p1, p1, 0x8

    const v2, 0x1000005

    add-int/2addr p2, v2

    add-int/2addr p2, p1

    .line 729
    invoke-virtual {p0, p2, v1}, Landroid/media/audiofx/AudioEffect;->getParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    const/4 p0, 0x0

    .line 730
    invoke-static {v1, p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->byteArrayToInt32Array([BII)[I

    move-result-object p0

    return-object p0

    .line 713
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "ERROR in getDapParameter(): Invalid profile index"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public getDapParameter(III)[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    const-string v0, "DolbyAudioEffect"

    if-ltz p1, :cond_1

    .line 744
    sget v1, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    if-ge p1, v1, :cond_1

    .line 748
    sget-object v1, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v1}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v1

    if-lt p2, v1, :cond_0

    sget-object v1, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v1}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v1

    if-gt p2, v1, :cond_0

    .line 755
    :try_start_0
    sget-object v0, Lcom/dolby/dax/DsParams;->kParamToLen:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x1

    :goto_0
    add-int/lit8 v1, v0, 0x2

    mul-int/lit8 v1, v1, 0x4

    .line 760
    new-array v1, v1, [B

    shl-int/lit8 p3, p3, 0x10

    shl-int/lit8 p2, p2, 0x8

    shl-int/lit8 p1, p1, 0xc

    or-int/2addr p1, p2

    const p2, 0x2000005

    add-int/2addr p3, p2

    add-int/2addr p3, p1

    .line 766
    invoke-virtual {p0, p3, v1}, Landroid/media/audiofx/AudioEffect;->getParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    const/4 p0, 0x0

    .line 767
    invoke-static {v1, p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->byteArrayToInt32Array([BII)[I

    move-result-object p0

    return-object p0

    .line 749
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "ERROR in getDapParameter(): Invalid port"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0

    .line 745
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "ERROR in getDapParameter(): Invalid profile index"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public getDialogEnhancerAmount()I
    .locals 1

    .line 368
    sget-object v0, Lcom/dolby/dax/DsParams;->DialogEnhancementAmount:Lcom/dolby/dax/DsParams;

    invoke-virtual {v0}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->getDapParameter(I)[I

    move-result-object p0

    const/4 v0, 0x0

    .line 369
    aget p0, p0, v0

    return p0
.end method

.method public getDialogEnhancerEnabled()Z
    .locals 1

    .line 340
    sget-object v0, Lcom/dolby/dax/DsParams;->DialogEnhancementEnable:Lcom/dolby/dax/DsParams;

    invoke-virtual {v0}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->getDapParameter(I)[I

    move-result-object p0

    const/4 v0, 0x0

    .line 341
    aget p0, p0, v0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getDsOn()Z
    .locals 1

    const/4 v0, 0x0

    .line 282
    invoke-virtual {p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->getBoolParam(I)Z

    move-result p0

    return p0
.end method

.method public getDsVersion()Ljava/lang/String;
    .locals 1

    .line 283
    const-string v0, "DAX3"

    return-object v0
.end method

.method public getGeqBandGains()[I
    .locals 1

    .line 527
    sget-object v0, Lcom/dolby/dax/DsParams;->GraphicEqualizerBandGains:Lcom/dolby/dax/DsParams;

    invoke-virtual {v0}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->getDapParameter(I)[I

    move-result-object p0

    return-object p0
.end method

.method public getIeqPreset()I
    .locals 1

    .line 486
    sget-object v0, Lcom/dolby/dax/DsParams;->IntelligentEqualizerPreset:Lcom/dolby/dax/DsParams;

    invoke-virtual {v0}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->getDapParameter(I)[I

    move-result-object p0

    const/4 v0, 0x0

    .line 487
    aget p0, p0, v0

    return p0
.end method

.method protected getIntParam(I)I
    .locals 2

    const/16 v0, 0xc

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 243
    invoke-static {p1, v0, v1}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    add-int/lit8 p1, p1, 0x5

    .line 244
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->getParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    .line 246
    invoke-static {v0}, Lcom/dolby/dax/DolbyAudioEffect;->byteArrayToInt32([B)I

    move-result p0

    return p0
.end method

.method public getNumOfProfiles()I
    .locals 1

    const/high16 v0, 0x3000000

    .line 562
    invoke-virtual {p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->getIntParam(I)I

    move-result p0

    return p0
.end method

.method public getProfile()I
    .locals 1

    const/high16 v0, 0xa000000

    .line 552
    invoke-virtual {p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->getIntParam(I)I

    move-result p0

    return p0
.end method

.method public getSelectedTuningDevice(I)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 872
    sget-object v0, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-lt p1, v0, :cond_0

    sget-object v0, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 876
    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->getPortDeviceNameLength(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    shr-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x4

    .line 878
    new-array v0, v0, [B

    shl-int/lit8 p1, p1, 0x10

    add-int/lit8 p1, p1, 0x4

    .line 880
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->getParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    .line 882
    :try_start_0
    new-instance p0, Ljava/lang/String;

    const-string p1, "UTF-8"

    invoke-direct {p0, v0, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string p1, "\\s+"

    .line 883
    invoke-virtual {p0, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    .line 884
    aget-object p0, p0, p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 886
    throw p0

    .line 873
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in getSelectedTuningDevice(): Invalid port"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public getTuningDevicesList(I)[Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 842
    sget-object v0, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-lt p1, v0, :cond_0

    sget-object v0, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 847
    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->getPortDeviceNameLength(I)I

    move-result v0

    add-int/lit8 v1, v0, 0x4

    shr-int/lit8 v1, v1, 0x2

    mul-int/lit8 v1, v1, 0x4

    .line 849
    new-array v1, v1, [B

    shl-int/lit8 p1, p1, 0x10

    add-int/lit8 p1, p1, 0x1

    .line 851
    invoke-virtual {p0, p1, v1}, Landroid/media/audiofx/AudioEffect;->getParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    .line 854
    :try_start_0
    new-instance p0, Ljava/lang/String;

    const-string p1, "UTF-8"

    invoke-direct {p0, v1, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/4 p1, 0x0

    add-int/lit8 v0, v0, -0x1

    .line 855
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    const-string p1, "\\s+"

    .line 856
    invoke-virtual {p0, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 859
    throw p0

    .line 843
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in getTuningDevicesList(): Invalid port"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public isGeqEnabled(I)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 498
    sget-object v0, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-lt p1, v0, :cond_1

    sget-object v0, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 503
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getProfile()I

    move-result v0

    sget-object v1, Lcom/dolby/dax/DsParams;->GraphicEqualizerEnable:Lcom/dolby/dax/DsParams;

    invoke-virtual {v1}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result v1

    invoke-virtual {p0, v0, p1, v1}, Lcom/dolby/dax/DolbyAudioEffect;->getDapParameter(III)[I

    move-result-object p0

    const/4 p1, 0x0

    .line 504
    aget p0, p0, p1

    if-eqz p0, :cond_0

    const/4 p1, 0x1

    :cond_0
    return p1

    .line 499
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in isGeqEnabled(): Invalid port "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public isMonoSpeaker()Z
    .locals 1

    const/high16 v0, 0xd000000

    .line 595
    invoke-virtual {p0, v0}, Lcom/dolby/dax/DolbyAudioEffect;->getBoolParam(I)Z

    move-result p0

    return p0
.end method

.method public isProfileSpecificSettingsModified(I)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-ltz p1, :cond_1

    .line 779
    sget v0, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    if-ge p1, v0, :cond_1

    const/16 v0, 0xc

    new-array v0, v0, [B

    shl-int/lit8 p1, p1, 0x10

    const v1, 0xb000005

    add-int/2addr p1, v1

    .line 788
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->getParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    .line 789
    invoke-static {v0}, Lcom/dolby/dax/DolbyAudioEffect;->byteArrayToInt32([B)I

    move-result p0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0

    .line 780
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in isProfileSpecificSettingsModified(): Invalid profile index"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public resetProfileSpecificSettings(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-ltz p1, :cond_0

    .line 801
    sget v0, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    if-ge p1, v0, :cond_0

    const/high16 v0, 0xc000000

    .line 806
    invoke-virtual {p0, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->setIntParam(II)I

    return-void

    .line 802
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in resetProfileSpecificSettings(): Invalid profile index"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method protected setBoolParam(IZ)I
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 185
    invoke-static {p1, v0, v1}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result p1

    add-int/2addr p1, v1

    const/4 v2, 0x1

    .line 187
    invoke-static {v2, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result v2

    add-int/2addr p1, v2

    .line 189
    invoke-static {p2, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    const/4 p1, 0x5

    .line 191
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->setParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    return v1
.end method

.method public setDapParameter(II[I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-ltz p1, :cond_2

    .line 621
    sget v0, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    if-ge p1, v0, :cond_2

    .line 626
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->isMonoSpeaker()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/dolby/dax/DsParams;->DolbyVirtualSpeakerVirtualizerControl:Lcom/dolby/dax/DsParams;

    invoke-virtual {v0}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result v0

    if-eq p2, v0, :cond_0

    goto :goto_0

    .line 627
    :cond_0
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p0

    .line 630
    :cond_1
    :goto_0
    array-length v0, p3

    add-int/lit8 v1, v0, 0x4

    mul-int/lit8 v1, v1, 0x4

    .line 631
    new-array v1, v1, [B

    const/4 v2, 0x0

    const/high16 v3, 0x1000000

    .line 634
    invoke-static {v3, v1, v2}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result v3

    add-int/2addr v3, v2

    add-int/lit8 v0, v0, 0x1

    .line 636
    invoke-static {v0, v1, v3}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result v0

    add-int/2addr v3, v0

    .line 638
    invoke-static {p1, v1, v3}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result p1

    add-int/2addr v3, p1

    .line 640
    invoke-static {p2, v1, v3}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result p1

    add-int/2addr v3, p1

    .line 642
    invoke-static {p3, v1, v3}, Lcom/dolby/dax/DolbyAudioEffect;->int32ArrayToByteArray([I[BI)I

    const/4 p1, 0x5

    .line 644
    invoke-virtual {p0, p1, v1}, Landroid/media/audiofx/AudioEffect;->setParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    return-void

    .line 622
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "ERROR in setProfileParameter(): Invalid profile index"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public setDapParameter(I[I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 608
    invoke-virtual {p0}, Lcom/dolby/dax/DolbyAudioEffect;->getProfile()I

    move-result v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/dolby/dax/DolbyAudioEffect;->setDapParameter(II[I)V

    return-void
.end method

.method public setDialogEnhancerAmount(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-ltz p1, :cond_0

    const/16 v0, 0x10

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 359
    sget-object p1, Lcom/dolby/dax/DsParams;->DialogEnhancementAmount:Lcom/dolby/dax/DsParams;

    invoke-virtual {p1}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result p1

    invoke-virtual {p0, p1, v0}, Lcom/dolby/dax/DolbyAudioEffect;->setDapParameter(I[I)V

    return-void

    .line 354
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in setDialogEnhancerAmount(): Invalid amount "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public setDialogEnhancerEnabled(Z)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 331
    sget-object p1, Lcom/dolby/dax/DsParams;->DialogEnhancementEnable:Lcom/dolby/dax/DsParams;

    invoke-virtual {p1}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result p1

    invoke-virtual {p0, p1, v0}, Lcom/dolby/dax/DolbyAudioEffect;->setDapParameter(I[I)V

    return-void
.end method

.method public setDsOn(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 272
    invoke-virtual {p0, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->setBoolParam(IZ)I

    .line 273
    invoke-super {p0, p1}, Landroid/media/audiofx/AudioEffect;->setEnabled(Z)I

    return-void
.end method

.method public setGeqBandGains([I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 514
    array-length v0, p1

    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    .line 518
    sget-object v0, Lcom/dolby/dax/DsParams;->GraphicEqualizerBandGains:Lcom/dolby/dax/DsParams;

    invoke-virtual {v0}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->setDapParameter(I[I)V

    return-void

    :cond_0
    const-string p0, "DolbyAudioEffect"

    const-string p1, "ERROR in setGeqBandGains(): Invalid GEq gains for 20 frequency bands!"

    .line 515
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public setIeqPreset(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 477
    sget-object p1, Lcom/dolby/dax/DsParams;->IntelligentEqualizerPreset:Lcom/dolby/dax/DsParams;

    invoke-virtual {p1}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result p1

    invoke-virtual {p0, p1, v0}, Lcom/dolby/dax/DolbyAudioEffect;->setDapParameter(I[I)V

    return-void

    .line 472
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in setIeqPreset(): Invalid IEq preset index"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method protected setIntParam(II)I
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 208
    invoke-static {p1, v0, v1}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result p1

    add-int/2addr p1, v1

    const/4 v2, 0x1

    .line 210
    invoke-static {v2, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result v2

    add-int/2addr p1, v2

    .line 212
    invoke-static {p2, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    const/4 p1, 0x5

    .line 214
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->setParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    return v1
.end method

.method public setProfile(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-ltz p1, :cond_0

    .line 538
    sget v0, Lcom/dolby/dax/DolbyAudioEffect;->sNumOfProfiles:I

    if-ge p1, v0, :cond_0

    const/high16 v0, 0xa000000

    .line 543
    invoke-virtual {p0, v0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->setIntParam(II)I

    return-void

    .line 539
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ERROR in setProfile(): Invalid profile index"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method public setSelectedTuningDevice(ILjava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 898
    sget-object v0, Lcom/dolby/dax/DsTuning;->internal_speaker:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-lt p1, v0, :cond_0

    sget-object v0, Lcom/dolby/dax/DsTuning;->usb:Lcom/dolby/dax/DsTuning;

    invoke-virtual {v0}, Lcom/dolby/dax/DsTuning;->toInt()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 904
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v1, v0, 0x4

    .line 905
    new-array v1, v1, [B

    const/4 v2, 0x0

    .line 908
    invoke-static {p1, v1, v2}, Lcom/dolby/dax/DolbyAudioEffect;->int32ToByteArray(I[BI)I

    move-result p1

    add-int/2addr p1, v2

    .line 910
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    invoke-static {p2, v2, v1, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 p1, 0x4

    .line 912
    invoke-virtual {p0, p1, v1}, Landroid/media/audiofx/AudioEffect;->setParameter(I[B)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/dolby/dax/DolbyAudioEffect;->checkReturnValue(I)V

    return-void

    .line 899
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "ERROR in setSelectedTuningDevice(): Invalid port"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "DolbyAudioEffect"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method
