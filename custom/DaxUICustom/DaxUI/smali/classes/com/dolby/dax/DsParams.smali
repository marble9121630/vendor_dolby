.class public final enum Lcom/dolby/dax/DsParams;
.super Ljava/lang/Enum;
.source "DsParams.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/dolby/dax/DsParams;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dolby/dax/DsParams;

.field public static final enum BassEnable:Lcom/dolby/dax/DsParams;

.field private static final DAP_PARAM_NAMES:[Ljava/lang/String;

.field public static final enum DialogEnhancementAmount:Lcom/dolby/dax/DsParams;

.field public static final enum DialogEnhancementDucking:Lcom/dolby/dax/DsParams;

.field public static final enum DialogEnhancementEnable:Lcom/dolby/dax/DsParams;

.field public static final enum DolbyHeadphoneVirtualizerControl:Lcom/dolby/dax/DsParams;

.field public static final enum DolbyVirtualSpeakerVirtualizerControl:Lcom/dolby/dax/DsParams;

.field public static final enum DolbyVolumeLevelerEnable:Lcom/dolby/dax/DsParams;

.field public static final enum GraphicEqualizerBandGains:Lcom/dolby/dax/DsParams;

.field public static final enum GraphicEqualizerEnable:Lcom/dolby/dax/DsParams;

.field public static final enum IntelligentEqualizerAmount:Lcom/dolby/dax/DsParams;

.field public static final enum IntelligentEqualizerPreset:Lcom/dolby/dax/DsParams;

.field public static final kParamToLen:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final params:[Lcom/dolby/dax/DsParams;


# instance fields
.field private id_:I


# direct methods
.method static constructor <clinit>()V
    .locals 36

    .line 27
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/4 v1, 0x0

    const-string v2, "DolbyHeadphoneVirtualizerControl"

    const/16 v3, 0x65

    invoke-direct {v0, v2, v1, v3}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->DolbyHeadphoneVirtualizerControl:Lcom/dolby/dax/DsParams;

    .line 35
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/4 v2, 0x1

    const-string v3, "DolbyVirtualSpeakerVirtualizerControl"

    const/16 v4, 0x66

    invoke-direct {v0, v3, v2, v4}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->DolbyVirtualSpeakerVirtualizerControl:Lcom/dolby/dax/DsParams;

    .line 42
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/4 v3, 0x2

    const-string v4, "DolbyVolumeLevelerEnable"

    const/16 v5, 0x67

    invoke-direct {v0, v4, v3, v5}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->DolbyVolumeLevelerEnable:Lcom/dolby/dax/DsParams;

    .line 48
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/4 v4, 0x3

    const-string v5, "IntelligentEqualizerPreset"

    const/16 v6, 0x68

    invoke-direct {v0, v5, v4, v6}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->IntelligentEqualizerPreset:Lcom/dolby/dax/DsParams;

    .line 55
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/4 v5, 0x4

    const-string v6, "DialogEnhancementEnable"

    const/16 v7, 0x69

    invoke-direct {v0, v6, v5, v7}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->DialogEnhancementEnable:Lcom/dolby/dax/DsParams;

    .line 62
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/4 v6, 0x5

    const-string v7, "GraphicEqualizerEnable"

    const/16 v8, 0x6a

    invoke-direct {v0, v7, v6, v8}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->GraphicEqualizerEnable:Lcom/dolby/dax/DsParams;

    .line 67
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/4 v7, 0x6

    const-string v8, "IntelligentEqualizerAmount"

    const/16 v9, 0x6b

    invoke-direct {v0, v8, v7, v9}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->IntelligentEqualizerAmount:Lcom/dolby/dax/DsParams;

    .line 72
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/4 v8, 0x7

    const-string v9, "DialogEnhancementAmount"

    const/16 v10, 0x6c

    invoke-direct {v0, v9, v8, v10}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->DialogEnhancementAmount:Lcom/dolby/dax/DsParams;

    .line 77
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/16 v9, 0x8

    const-string v10, "DialogEnhancementDucking"

    const/16 v11, 0x6d

    invoke-direct {v0, v10, v9, v11}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->DialogEnhancementDucking:Lcom/dolby/dax/DsParams;

    .line 82
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/16 v10, 0x9

    const-string v11, "GraphicEqualizerBandGains"

    const/16 v12, 0x6e

    invoke-direct {v0, v11, v10, v12}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->GraphicEqualizerBandGains:Lcom/dolby/dax/DsParams;

    .line 89
    new-instance v0, Lcom/dolby/dax/DsParams;

    const/16 v11, 0xa

    const-string v12, "BassEnable"

    const/16 v13, 0x6f

    invoke-direct {v0, v12, v11, v13}, Lcom/dolby/dax/DsParams;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dolby/dax/DsParams;->BassEnable:Lcom/dolby/dax/DsParams;

    const/16 v0, 0xb

    new-array v12, v0, [Lcom/dolby/dax/DsParams;

    .line 17
    sget-object v13, Lcom/dolby/dax/DsParams;->DolbyHeadphoneVirtualizerControl:Lcom/dolby/dax/DsParams;

    aput-object v13, v12, v1

    sget-object v14, Lcom/dolby/dax/DsParams;->DolbyVirtualSpeakerVirtualizerControl:Lcom/dolby/dax/DsParams;

    aput-object v14, v12, v2

    sget-object v15, Lcom/dolby/dax/DsParams;->DolbyVolumeLevelerEnable:Lcom/dolby/dax/DsParams;

    aput-object v15, v12, v3

    sget-object v16, Lcom/dolby/dax/DsParams;->IntelligentEqualizerPreset:Lcom/dolby/dax/DsParams;

    aput-object v16, v12, v4

    sget-object v17, Lcom/dolby/dax/DsParams;->DialogEnhancementEnable:Lcom/dolby/dax/DsParams;

    aput-object v17, v12, v5

    sget-object v18, Lcom/dolby/dax/DsParams;->GraphicEqualizerEnable:Lcom/dolby/dax/DsParams;

    aput-object v18, v12, v6

    sget-object v19, Lcom/dolby/dax/DsParams;->IntelligentEqualizerAmount:Lcom/dolby/dax/DsParams;

    aput-object v19, v12, v7

    sget-object v20, Lcom/dolby/dax/DsParams;->DialogEnhancementAmount:Lcom/dolby/dax/DsParams;

    aput-object v20, v12, v8

    sget-object v21, Lcom/dolby/dax/DsParams;->DialogEnhancementDucking:Lcom/dolby/dax/DsParams;

    aput-object v21, v12, v9

    sget-object v22, Lcom/dolby/dax/DsParams;->GraphicEqualizerBandGains:Lcom/dolby/dax/DsParams;

    aput-object v22, v12, v10

    sget-object v23, Lcom/dolby/dax/DsParams;->BassEnable:Lcom/dolby/dax/DsParams;

    aput-object v23, v12, v11

    sput-object v12, Lcom/dolby/dax/DsParams;->$VALUES:[Lcom/dolby/dax/DsParams;

    const-string v24, "null"

    const-string v25, "vdhe"

    const-string v26, "vspe"

    const-string v27, "dvle"

    const-string v28, "ieid"

    const-string v29, "deon"

    const-string v30, "geon"

    const-string v31, "iea"

    const-string v32, "dea"

    const-string v33, "ded"

    const-string v34, "gebg"

    const-string v35, "beon"

    .line 148
    filled-new-array/range {v24 .. v35}, [Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/dolby/dax/DsParams;->DAP_PARAM_NAMES:[Ljava/lang/String;

    new-array v0, v0, [Lcom/dolby/dax/DsParams;

    aput-object v13, v0, v1

    aput-object v14, v0, v2

    aput-object v15, v0, v3

    aput-object v16, v0, v4

    aput-object v17, v0, v5

    aput-object v18, v0, v6

    aput-object v19, v0, v7

    aput-object v20, v0, v8

    aput-object v21, v0, v9

    aput-object v22, v0, v10

    aput-object v23, v0, v11

    .line 153
    sput-object v0, Lcom/dolby/dax/DsParams;->params:[Lcom/dolby/dax/DsParams;

    .line 233
    new-instance v0, Lcom/dolby/dax/DsParams$1;

    invoke-direct {v0}, Lcom/dolby/dax/DsParams$1;-><init>()V

    sput-object v0, Lcom/dolby/dax/DsParams;->kParamToLen:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dolby/dax/DsParams;->id_:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dolby/dax/DsParams;
    .locals 1

    .line 17
    const-class v0, Lcom/dolby/dax/DsParams;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/dolby/dax/DsParams;

    return-object p0
.end method

.method public static values()[Lcom/dolby/dax/DsParams;
    .locals 1

    .line 17
    sget-object v0, Lcom/dolby/dax/DsParams;->$VALUES:[Lcom/dolby/dax/DsParams;

    invoke-virtual {v0}, [Lcom/dolby/dax/DsParams;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dolby/dax/DsParams;

    return-object v0
.end method


# virtual methods
.method public toInt()I
    .locals 0

    .line 98
    iget p0, p0, Lcom/dolby/dax/DsParams;->id_:I

    return p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 106
    iget p0, p0, Lcom/dolby/dax/DsParams;->id_:I

    const/16 v0, 0x64

    if-le p0, v0, :cond_0

    const/16 v1, 0x70

    if-ge p0, v1, :cond_0

    .line 107
    sget-object v1, Lcom/dolby/dax/DsParams;->DAP_PARAM_NAMES:[Ljava/lang/String;

    sub-int/2addr p0, v0

    aget-object p0, v1, p0

    goto :goto_0

    :cond_0
    const-string p0, "error"

    :goto_0
    return-object p0
.end method
